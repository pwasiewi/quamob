# **QuasarSpectrum**  

## Description: ##
The main purpose of the application is to connect with the QuasarRestServer and provide and easier way to view data and execute parametrization requests using Quasar application (https://bitbucket.org/jszuppe/quasar). 

## Technologies used: ##
* Java SE 1.8 64bit
* Spring Framework's RestTemplate - Executing HTTP message calls.
* PhilJay MPAndroidChart - Displaying astronomical object spectrum on a chart.
* FasterXML Jackson - Serializing/deserializing JSON.

## Info: ##
* Some bugs and improvements are expected. 
* The code needs to be cleaned up. 

