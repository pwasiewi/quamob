package com.kp.quasarspectrum.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.kp.quasarspectrum.BuildConfig;
import com.kp.quasarspectrum.Interface.Constants;
import com.kp.quasarspectrum.R;

/**
 * Created by administrator on 20.05.16.
 */
public class SettingsActivity extends Activity {

    private String QRSaddress;
    private Integer PageSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Set dbaddress
        QRSaddress = GetQRSAddress();

        // Set pagesize
        PageSize = GetPageSize();

        // DB address
        EditText settings_edittext_dbaddress = (EditText) findViewById(R.id.settings_edittext_dbaddress);
        settings_edittext_dbaddress.setText(QRSaddress);
        settings_edittext_dbaddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                QRSaddress = s.toString();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
        InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm1.showSoftInput(settings_edittext_dbaddress, InputMethodManager.SHOW_IMPLICIT);

        // Page size
        EditText settings_edittext_pagesize = (EditText) findViewById(R.id.settings_edittext_pagesize);
        settings_edittext_pagesize.setText(PageSize.toString());
        settings_edittext_pagesize.addTextChangedListener( new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    PageSize = Integer.parseInt(s.toString());
                    if (PageSize <= 0) throw new Exception();
                }
                catch (NumberFormatException e) {
                    if (BuildConfig.DEBUG) {
                        Log.e(Constants.LOG, "Page size parse error. Not a valid integer: " + s.toString());
                    }
                }
                catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.e(Constants.LOG, "Page size parse error. Integer <= 0: " + s.toString());
                    }
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
        InputMethodManager imm2 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm2.showSoftInput(settings_edittext_pagesize, InputMethodManager.SHOW_IMPLICIT);

        // Save button controller
        Button settings_button_save = (Button) findViewById(R.id.settings_button_save);
        settings_button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SetQRSAddress(QRSaddress);
                SetPageSize(PageSize);
            }
        });
    }

    private String GetQRSAddress(){
        return getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE).getString("url", "");
    }

    private void SetQRSAddress(String str){
        if (str != null) {
            SharedPreferences sp = getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE);
            SharedPreferences.Editor e = sp.edit();
            e.putString("url", str);
            e.apply();

            if (BuildConfig.DEBUG) {
                Log.i(Constants.LOG, "QRS base url set to: " + str);
            }
        }
        else if (BuildConfig.DEBUG) { Log.e(Constants.LOG, "New QRS base url is null"); }
    }

    private Integer GetPageSize(){
        return getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE).getInt("pagesize", 100);
    }

    private void SetPageSize(Integer val){
        if (val != null) {
            if (val <= 0) {
                if (BuildConfig.DEBUG) {
                    Log.e(Constants.LOG, "Page size error. Integer <= 0: " + val.toString());
                }
                return;
            }
            SharedPreferences sp = getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE);
            SharedPreferences.Editor e = sp.edit();
            e.putInt("pagesize", val);
            e.apply();

            if (BuildConfig.DEBUG) {
                Log.i(Constants.LOG, "Page size set to: " + val);
            }
        }
        else if (BuildConfig.DEBUG) { Log.e(Constants.LOG, "Page size is null"); }
    }


}
