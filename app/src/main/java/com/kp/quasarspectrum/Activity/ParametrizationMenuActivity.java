package com.kp.quasarspectrum.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kp.quasarspectrum.Adapter.ParaResultSetAdapter;
import com.kp.quasarspectrum.BuildConfig;
import com.kp.quasarspectrum.Interface.AsyncResponse;
import com.kp.quasarspectrum.Interface.Constants;
import com.kp.quasarspectrum.Misc.RequestParams;
import com.kp.quasarspectrum.Model.Command.Command;
import com.kp.quasarspectrum.Model.Command.SpectralLine;
import com.kp.quasarspectrum.Model.ParaResult;
import com.kp.quasarspectrum.Model.ParaResultSet;
import com.kp.quasarspectrum.Model.QuasarSet;
import com.kp.quasarspectrum.R;
import com.kp.quasarspectrum.Task.ParametrizationTask;
import com.kp.quasarspectrum.Task.QRSTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by administrator on 07.06.16.
 */
public class ParametrizationMenuActivity extends Activity implements AsyncResponse<Long> {

    private List<ParaResult> paraResultList = new ArrayList<ParaResult>();
    private List<ParaResultSet> paraResultSetList = new ArrayList<ParaResultSet>();
    private QuasarSet quasarSet = null;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametrizationmenu);

        final String quasar_set_oid = getIntent().getStringExtra("quasar_set_oid");

        // Setting progress bar
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_spinner);

        // Using custom adapter
        final ParaResultSetAdapter lwAdapter = new ParaResultSetAdapter(this, paraResultSetList);

        // Using custom listView
        final ListView paraResultSetlistView = (ListView) findViewById(R.id.pararesultset_listView);

        // Get QuasarSet data
        getQuasarBySetId(progressBar, quasar_set_oid, lwAdapter, paraResultSetlistView);

        // Setting on click action. Should direct to quasar list.
        paraResultSetlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(view.getContext(), ParaResultListActivity.class);
                final int result = 1;
                ParaResultSet obj = (ParaResultSet) adapterView.getItemAtPosition(i);
                intent.putExtra("para_result_set_oid", obj.getId());
                startActivityForResult(intent, result);
            }}
        );

        // Parametrization options button
        Button parametrizationmenu_button_options = (Button) findViewById(R.id.parametrizationmenu_button_options);
        parametrizationmenu_button_options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (quasarSet != null) {
                    Intent intent = new Intent(view.getContext(), ParametrizationOptionsActivity.class);
                    final int result = 1;
                    intent.putExtra("setName", quasarSet.getName());
                    startActivityForResult(intent, result);
                }
            }
        });

        // Parametrization button
        Button parametrizationmenu_button_parametrize = (Button) findViewById(R.id.parametrizationmenu_button_parametrize);
        parametrizationmenu_button_parametrize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartParametrizationTask(progressBar, lwAdapter, paraResultSetlistView);
            }
        });
    }



    // ToDo read parametrization options from preferences
    private void StartParametrizationTask(ProgressBar progressBar, final ParaResultSetAdapter adapter, final ListView listView){
        if (quasarSet != null) {
            // Set address to query (required)
            SharedPreferences sp = getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE);
            String urlQRS = sp.getString("url", "");
            if (!urlQRS.endsWith("/")) urlQRS  += "/";
            urlQRS += "command";


            // Set useDefault parameter (required)
            Boolean useDefault = getSharedPreferences("quasarSpectrumParametrizationOptionsEnabler", Context.MODE_PRIVATE).getBoolean("useDefault", true);
            if (useDefault) {
                if(BuildConfig.DEBUG) Log.i(Constants.LOG, "Using default parametrization options");

                // Create default command (required)
                Command command = new Command(true);
                // Set set name (required)
                command.setSetName(quasarSet.getName());
                // Execute task
                taskParametrize(progressBar, command, urlQRS, adapter, listView);
            }
            else {
                if(BuildConfig.DEBUG) Log.i(Constants.LOG, "Using custom parametrization options");

                // Create custom command (required)
                Command command = ParseCommandOptions();
                // Set set name (required)
                command.setSetName(quasarSet.getName());
                // Execute task
                taskParametrize(progressBar, command, urlQRS, adapter, listView);
            }
        }
        //else // ToDo what??
    }

    private void taskParametrize(final ProgressBar progressBar, Command command, String url, final ParaResultSetAdapter adapter, final ListView listView){
        if (progressBar != null) progressBar.setVisibility(View.VISIBLE);
        new ParametrizationTask(new AsyncResponse<String>(){
            @Override
            public void processFinish(String output){
                if (output != null) {
                    if (BuildConfig.DEBUG) {
                        Log.d(Constants.LOG, "AsyncResponse returned string: " + output);
                    }

                    // Show toast
                    Toast.makeText(context, "Parametrization completed",Toast.LENGTH_LONG).show();

                    // Get new parametrization task list:
                    getParaResultSetListBySetName(progressBar, quasarSet.getName(), adapter, listView);
                }
                if (progressBar != null) progressBar.setVisibility(View.GONE);
            }
        }, command, url).execute();
    }

    private Command ParseCommandOptions() {
        SharedPreferences spPOEnabler = getSharedPreferences("quasarSpectrumParametrizationOptionsEnabler", Context.MODE_PRIVATE);
        SharedPreferences spPO = getSharedPreferences("quasarSpectrumParametrizationOptions", Context.MODE_PRIVATE);
        Command command = new Command(false); // Command is definitely using custom parametrization options
        Integer successfull = 0;
        Integer total = 0;



        // Try parse parametrization options
        if (!spPOEnabler.getBoolean("spectralLines", true)){
            total++;
            try{
                ObjectMapper mapper = new ObjectMapper();
                //mapper.canSerialize(Command.class);
                SpectralLine[] parsed = mapper.readValue(spPO.getString("spectralLines", ""), SpectralLine[].class);
                if (parsed != null)
                {
                    if(BuildConfig.DEBUG) Log.i(Constants.LOG, "spectralLines parsed");
                    command.setSpectralLines(parsed);
                    successfull++;
                }
                else
                    if(BuildConfig.DEBUG) Log.w(Constants.LOG, "spectralLines parsing failed. Using default.");
            }
            catch (JsonGenerationException e)
            {
                HandleException(e);
            }
            catch (IOException e)
            {
                HandleException(e);
            }
        }

        if (!spPOEnabler.getBoolean("continuumWindows", true)){
            total++;
            try{
                ObjectMapper mapper = new ObjectMapper();
                Float[][] parsed = mapper.readValue(spPO.getString("continuumWindows", ""), Float[][].class);
                if (parsed != null)
                {
                    if(BuildConfig.DEBUG) Log.i(Constants.LOG, "continuumWindows parsed");
                    command.setContinuumWindows(parsed);
                    successfull++;
                }
                else
                    if(BuildConfig.DEBUG) Log.w(Constants.LOG, "continuumWindows parsing failed. Using default.");
            }
            catch (JsonGenerationException e)
            {
                HandleException(e);
            }
            catch (IOException e)
            {
                HandleException(e);
            }
        }

        if (!spPOEnabler.getBoolean("ampWl", true)){
            total++;
            try{
                ObjectMapper mapper = new ObjectMapper();
                Float parsed = mapper.readValue(spPO.getString("ampWl", ""), Float.class);
                if (parsed != null)
                {
                    if(BuildConfig.DEBUG) Log.i(Constants.LOG, "ampWl parsed");
                    command.setAmpWl(parsed);
                    successfull++;
                }
                else
                    if(BuildConfig.DEBUG) Log.w(Constants.LOG, "ampWl parsing failed. Using default.");
            }
            catch (JsonGenerationException e)
            {
                HandleException(e);
            }
            catch (IOException e)
            {
                HandleException(e);
            }
        }

        if (!spPOEnabler.getBoolean("feWindows", true)){
            total++;
            try{
                ObjectMapper mapper = new ObjectMapper();
                Float[][] parsed = mapper.readValue(spPO.getString("feWindows", ""), Float[][].class);
                if (parsed != null)
                {
                    if(BuildConfig.DEBUG) Log.i(Constants.LOG, "feWindows parsed");
                    command.setFeWindows(parsed);
                    successfull++;
                }
                else
                    if(BuildConfig.DEBUG) Log.w(Constants.LOG, "feWindows parsing failed. Using default.");
            }
            catch (JsonGenerationException e)
            {
                HandleException(e);
            }
            catch (IOException e)
            {
                HandleException(e);
            }
        }

        if (!spPOEnabler.getBoolean("feTemp", true)){
            total++;
            try{
                ObjectMapper mapper = new ObjectMapper();
                Float[][] parsed = mapper.readValue(spPO.getString("feTemp", ""), Float[][].class);
                if (parsed != null)
                {
                    if(BuildConfig.DEBUG) Log.i(Constants.LOG, "feTemp parsed");
                    command.setFeTemp(parsed);
                    successfull++;
                }
                else
                    if(BuildConfig.DEBUG) Log.w(Constants.LOG, "feTemp parsing failed. Using default.");
            }
            catch (JsonGenerationException e)
            {
                HandleException(e);
            }
            catch (IOException e)
            {
                HandleException(e);
            }
        }

        if (!spPOEnabler.getBoolean("feFwhmn", true)){
            total++;
            try{
                ObjectMapper mapper = new ObjectMapper();
                Float parsed = mapper.readValue(spPO.getString("feFwhmn", ""), Float.class);
                if (parsed != null)
                {
                    if(BuildConfig.DEBUG) Log.i(Constants.LOG, "feFwhmn parsed");
                    command.setFeFwhmn(parsed);
                    successfull++;
                }
                else
                    if(BuildConfig.DEBUG) Log.w(Constants.LOG, "feFwhmn parsing failed. Using default.");
            }
            catch (JsonGenerationException e)
            {
                HandleException(e);
            }
            catch (IOException e)
            {
                HandleException(e);
            }
        }

        if (!spPOEnabler.getBoolean("feFwhmt", true)){
            total++;
            try{
                ObjectMapper mapper = new ObjectMapper();
                Float parsed = mapper.readValue(spPO.getString("feFwhmt", ""), Float.class);
                if (parsed != null)
                {
                    if(BuildConfig.DEBUG) Log.i(Constants.LOG, "feFwhmt parsed");
                    command.setFeFwhmt(parsed);
                    successfull++;
                }
                else
                    if(BuildConfig.DEBUG) Log.w(Constants.LOG, "feFwhmt parsing failed. Using default.");
            }
            catch (JsonGenerationException e)
            {
                HandleException(e);
            }
            catch (IOException e)
            {
                HandleException(e);
            }
        }

        if (!spPOEnabler.getBoolean("feScaleRate", true)){
            total++;
            try{
                ObjectMapper mapper = new ObjectMapper();
                Float parsed = mapper.readValue(spPO.getString("feScaleRate", ""), Float.class);
                if (parsed != null)
                {
                    if(BuildConfig.DEBUG) Log.i(Constants.LOG, "feScaleRate parsed");
                    command.setFeScaleRate(parsed);
                    successfull++;
                }
                else
                    if(BuildConfig.DEBUG) Log.w(Constants.LOG, "feScaleRate parsing failed. Using default.");
            }
            catch (JsonGenerationException e)
            {
                HandleException(e);
            }
            catch (IOException e)
            {
                HandleException(e);
            }
        }

        if (!spPOEnabler.getBoolean("feFitRanges", true)){
            total++;
            try{
                ObjectMapper mapper = new ObjectMapper();
                Float[] parsed = mapper.readValue(spPO.getString("feFitRanges", ""), Float[].class);
                if (parsed != null)
                {
                    if(BuildConfig.DEBUG) Log.i(Constants.LOG, "feFitRanges parsed");
                    command.setFeFitRanges(parsed);
                    successfull++;
                }
                else
                    if(BuildConfig.DEBUG) Log.w(Constants.LOG, "feFitRanges parsing failed. Using default.");
            }
            catch (JsonGenerationException e)
            {
                HandleException(e);
            }
            catch (IOException e)
            {
                HandleException(e);
            }
        }

        if (!spPOEnabler.getBoolean("feFitType", true)){
            total++;
            try{
                ObjectMapper mapper = new ObjectMapper();
                Integer parsed = mapper.readValue(spPO.getString("feFitType", ""), Integer.class);
                if (parsed != null)
                {
                    if(BuildConfig.DEBUG) Log.i(Constants.LOG, "feFitType parsed");
                    command.setFeFitType(parsed);
                    successfull++;
                }
                else
                    if(BuildConfig.DEBUG) Log.w(Constants.LOG, "feFitType parsing failed. Using default.");
            }
            catch (JsonGenerationException e)
            {
                HandleException(e);
            }
            catch (IOException e)
            {
                HandleException(e);
            }
        }

        if (BuildConfig.DEBUG) Log.i(Constants.LOG, "Successfull parses: " + successfull + " out of " + total);

        if (successfull > 0)
            return command;
        else
            return new Command(true); // If all parses failed then we need to return Command object with useDefault = true;
    }

    private String SerializeCommand(Command command){
        try{
            ObjectMapper mapper = new ObjectMapper();
            //mapper.canSerialize(Command.class);
            String cmdJson = mapper.writeValueAsString(command);
            if (cmdJson != null)
            {
                if(BuildConfig.DEBUG) Log.i(Constants.LOG, "Command serialization completed: \n" + cmdJson);
                return cmdJson;
            }
            else
            if(BuildConfig.DEBUG) Log.i(Constants.LOG, "Command serialization failed");

            return null;
        }
        catch (JsonGenerationException e)
        {
            HandleException(e);
        }
        catch (JsonMappingException e)
        {
            HandleException(e);
        }
        catch (IOException e)
        {
            HandleException(e);
        }
        catch (Exception e)
        {
            HandleException(e);
        }
        return null; // Serialization failed
    }

    // ToDo improve this method
    private void HandleException(Exception e){
        if(BuildConfig.DEBUG) Log.e(Constants.LOG, "spectralLines parsing exception. Using default.");
        // ToDo display warning?
    }

    private void getQuasarBySetId(ProgressBar progressBar, final String id, ParaResultSetAdapter adapter, ListView listView){
        SharedPreferences sp = getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE);
        String urlQRS = sp.getString("url", "");
        RequestParams rp = new RequestParams(urlQRS, "quasarset", "byid", new HashMap<String, String>() {{
            put("id", id);
        }});
        taskQuasarSet(progressBar, rp, adapter, listView);
    }

    private void taskQuasarSet(final ProgressBar progressBar, RequestParams rp, final ParaResultSetAdapter adapter, final ListView listView){
        if (progressBar != null) progressBar.setVisibility(View.VISIBLE);
        new QRSTask<QuasarSet>(new AsyncResponse<QuasarSet>(){
            @Override
            public void processFinish(QuasarSet output){
                if (output != null) {
                    quasarSet = output;
                    if (BuildConfig.DEBUG) {
                        Log.d(Constants.LOG, "AsyncResponse returned object: " + quasarSet.toString());
                    }

                    // Set QuasarSet data
                    TextView textView1 = (TextView) findViewById(R.id.parametrizationmenu_textview_quasarset);
                    textView1.setText(quasarSet.toString());

                    // ToDo find ParaResultSet list
                    // a) for this quasarSet.name
                    // b) from user preferences (past tasks)

                    // a: - Temporary?
                    getParaResultSetListBySetName(progressBar, quasarSet.getName(), adapter, listView);
                }
                if (progressBar != null) progressBar.setVisibility(View.GONE);
            }
        }, rp).execute();
    }

    private void getParaResultSetListBySetName(ProgressBar progressBar, final String setName, final ParaResultSetAdapter adapter, final ListView listView){
        SharedPreferences sp = getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE);
        String urlQRS = sp.getString("url", "");
        RequestParams rp = new RequestParams(urlQRS, "pararesultset", "bysetname", new HashMap<String, String>() {{
            put("set_name", setName);
        }}, 0, Integer.MAX_VALUE);
        taskParaResultSetList(progressBar, rp, adapter, listView);
    }

    private void taskParaResultSetList(final ProgressBar progressBar, RequestParams rp, final ParaResultSetAdapter adapter, final ListView listView){
        if (progressBar != null) progressBar.setVisibility(View.VISIBLE);
        new QRSTask<List<ParaResultSet>>(new AsyncResponse<List<ParaResultSet>>(){
            @Override
            public void processFinish(List<ParaResultSet> output){
                if (output != null) {
                    if (BuildConfig.DEBUG) {
                        Log.d(Constants.LOG, "AsyncResponse returned size = " + output.size());
                    }
                    paraResultSetList.clear();
                    paraResultSetList.addAll(output);
                    listView.setAdapter(adapter);
                }
                if (progressBar != null) progressBar.setVisibility(View.GONE);
            }
        }, rp).execute();
    }

    /**
     * This method will not be called but has to be present in application.
     * @param output async task result
     */
    @Override
    public void processFinish(Long output) {

    }
}
