package com.kp.quasarspectrum.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kp.quasarspectrum.Adapter.ElementFitsAdapter;
import com.kp.quasarspectrum.BuildConfig;
import com.kp.quasarspectrum.Interface.AsyncResponse;
import com.kp.quasarspectrum.Interface.Constants;
import com.kp.quasarspectrum.Misc.RequestParams;
import com.kp.quasarspectrum.Model.ParaResult;
import com.kp.quasarspectrum.Model.Property.ElementFits;
import com.kp.quasarspectrum.R;
import com.kp.quasarspectrum.Task.QRSTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by administrator on 09.06.16.
 */
public class ParaResultDetailsActivity extends Activity implements AsyncResponse<Long> {

    private ParaResult paraResult = null;
    private List<ElementFits> elementFitsList = new ArrayList<ElementFits>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pararesult_details);

        String id = getIntent().getStringExtra("id");
        if (BuildConfig.DEBUG) {
            Log.d(Constants.LOG, "Displaying details of pararesult: " + id);
        }

        // Setting progress bar
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_spinner);

        // Using custom adapter
        ElementFitsAdapter lwAdapter = new ElementFitsAdapter(this, elementFitsList);

        // Using custom listView
        ListView paraResultSetlistView = (ListView) findViewById(R.id.element_listView);

        // Applying the adapter
        //paraResultSetlistView.setAdapter(lwAdapter);

        // Getting Quasar details (full version of QuasarShorted)
        getParaResultById(progressBar, id, lwAdapter, paraResultSetlistView);

        // ToDo save to file?
    }

    private void getParaResultById(ProgressBar progressBar, final String id, final ElementFitsAdapter adapter, final ListView listView){
        SharedPreferences sp = getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE);
        String urlQRS = sp.getString("url", "");
        RequestParams rp = new RequestParams(urlQRS, "pararesult", "byid", new HashMap<String, String>() {{
            put("id", id);
        }});
        taskParaResult(progressBar, rp, adapter, listView);
    }

    private void taskParaResult(final ProgressBar progressBar, RequestParams rp, final ElementFitsAdapter adapter, final ListView listView){
        if (progressBar != null) progressBar.setVisibility(View.VISIBLE);
        new QRSTask<ParaResult>(new AsyncResponse<ParaResult>(){
            @Override
            public void processFinish(ParaResult output){
                if (output != null) {
                    paraResult = output;
                    if (BuildConfig.DEBUG) {
                        Log.d(Constants.LOG, "AsyncResponse returned object: " + output.toString());
                    }

                    // Updating data on layout
                    TextView textView0 = (TextView) findViewById(R.id.pararesult_details_value_id);
                    textView0.setText(paraResult.getId());

                    TextView textView1 = (TextView) findViewById(R.id.pararesult_details_value_setid);
                    textView1.setText(paraResult.getParaResultSetOid());

                    TextView textView2 = (TextView) findViewById(R.id.pararesult_details_value_mjd);
                    textView2.setText(String.format("%d", paraResult.getMjd()));

                    TextView textView3 = (TextView) findViewById(R.id.pararesult_details_value_plate);
                    textView3.setText(String.format("%d", paraResult.getPlate()));

                    TextView textView4 = (TextView) findViewById(R.id.pararesult_details_value_fiber);
                    textView4.setText(String.format("%d", paraResult.getFiber()));

                    TextView textView5 = (TextView) findViewById(R.id.pararesult_details_value_contchisq);
                    textView5.setText(String.format("%f", paraResult.getContChisq()));

                    TextView textView6 = (TextView) findViewById(R.id.pararesult_details_value_contreglinresult);
                    textView6.setText(paraResult.getContReglinResultString());

                    TextView textView7 = (TextView) findViewById(R.id.pararesult_details_value_reglinresult);
                    textView7.setText(paraResult.getReglinResultString());

                    TextView textView8 = (TextView) findViewById(R.id.pararesult_details_value_fescalerate);
                    textView8.setText(String.format("%f", paraResult.getFeScaleRate()));

                    TextView textView9 = (TextView) findViewById(R.id.pararesult_details_value_fewinssize);
                    textView9.setText(String.format("%d", paraResult.getFeWinsSize()));

                    TextView textView10 = (TextView) findViewById(R.id.pararesult_details_value_fewinsreducedchisq);
                    textView10.setText(String.format("%f", paraResult.getFeWinsReducedChisq()));

                    TextView textView11 = (TextView) findViewById(R.id.pararesult_details_value_fefullew);
                    textView11.setText(String.format("%f", paraResult.getFeFullEw()));

                    TextView textView12 = (TextView) findViewById(R.id.pararesult_details_value_ferangeew);
                    textView12.setText(String.format("%f", paraResult.getFeRangeEw()));

                    // ToDo display elements
                    elementFitsList.clear();
                    Collections.addAll(elementFitsList, paraResult.getElementsFits());
                    listView.setAdapter(adapter);
                }
                if (progressBar != null) progressBar.setVisibility(View.GONE);
            }
        }, rp).execute();
    }

    // ToDo implement this method in all db reliant tasks
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED;
    }

    /**
     * This method will not be called but has to be present in application.
     * @param output async task result
     */
    @Override
    public void processFinish(Long output) {

    }
}
