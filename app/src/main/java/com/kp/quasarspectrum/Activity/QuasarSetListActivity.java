package com.kp.quasarspectrum.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.kp.quasarspectrum.Adapter.QuasarSetAdapter;
import com.kp.quasarspectrum.BuildConfig;
import com.kp.quasarspectrum.Interface.AsyncResponse;
import com.kp.quasarspectrum.Interface.Constants;
import com.kp.quasarspectrum.Misc.RequestParams;
import com.kp.quasarspectrum.Model.QuasarSet;
import com.kp.quasarspectrum.R;
import com.kp.quasarspectrum.Task.QRSTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 03.06.16.
 */
public class QuasarSetListActivity extends Activity implements AsyncResponse<Long>{

    public List<QuasarSet> quasarSetList = new ArrayList<QuasarSet>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quasarset_list);

        // Setting progress bar
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_spinner);

        // Using custom adapter
        QuasarSetAdapter lwAdapter = new QuasarSetAdapter(this, quasarSetList);

        // Using custom listView
        ListView quasarSetlistView = (ListView) findViewById(R.id.quasarset_listView);

        // Applying the adapter
        //quasarSetlistView.setAdapter(lwAdapter);

        getQuasarSetList(progressBar, lwAdapter, quasarSetlistView);

        // Setting on click action. Should direct to quasar list.
        quasarSetlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //String tvShowPicked = "You selected " + String.valueOf(adapterView.getItemAtPosition(i));
                //Toast.makeText(QuasarSetListActivity.this, tvShowPicked, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(view.getContext(), QuasarShortedListActivity.class);
                final int result = 1;
                QuasarSet obj = (QuasarSet) adapterView.getItemAtPosition(i);
                intent.putExtra("quasar_set_oid", obj.getId());
                startActivityForResult(intent, result);
            }}
        );
    }

    private void getQuasarSetList(ProgressBar progressBar, final QuasarSetAdapter adapter, final ListView listView){
        SharedPreferences sp = getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE);
        String urlQRS = sp.getString("url", "");
        RequestParams rp = new RequestParams(urlQRS, "quasarset", 0, Integer.MAX_VALUE);
        taskQuasarSetList(progressBar, rp, adapter, listView);
    }

    private void taskQuasarSetList(final ProgressBar progressBar, RequestParams rp, final QuasarSetAdapter adapter, final ListView listView){
        if (progressBar != null) progressBar.setVisibility(View.VISIBLE);
        new QRSTask<List<QuasarSet>>(new AsyncResponse<List<QuasarSet>>(){
            @Override
            public void processFinish(List<QuasarSet> output){
                if (output != null) {
                    quasarSetList.clear();
                    quasarSetList.addAll(output);
                    listView.setAdapter(adapter);
                    if (BuildConfig.DEBUG) {
                        Log.d(Constants.LOG, "AsyncResponse returned size = " + output.size());
                    }
                }
                if (progressBar != null) progressBar.setVisibility(View.GONE);
            }
        }, rp).execute();
    }


    /**
     * This method will not be called but has to be present in application.
     * @param output async task result
     */
    @Override
    public void processFinish(Long output) {

    }
}
