package com.kp.quasarspectrum.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.kp.quasarspectrum.BuildConfig;
import com.kp.quasarspectrum.Interface.AsyncResponse;
import com.kp.quasarspectrum.Interface.Constants;
import com.kp.quasarspectrum.Misc.Network;
import com.kp.quasarspectrum.R;
import com.kp.quasarspectrum.Task.CountTask;

//import android.support.v7.app.AppCompatActivity;

// Not used - standard version of sfl4j + logback
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;


public class MainActivity extends Activity implements AsyncResponse<Long>
{
    private String urlQRS;  // Read from shared preferences

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    public void onResume() {
        super.onResume();
        if (BuildConfig.DEBUG) { Log.d(this.getLocalClassName(), "onResume called"); }
    }

    public void initData(){
        readSettings();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setActionBar(toolbar);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_spinner);

        // Checking network connectivity state
        if (!Network.isOnline(this)) {
            Toast.makeText(this, "Network not available",Toast.LENGTH_LONG).show();
            return;
        }

        setDBStats(urlQRS, progressBar);

    }

    /**
     * This method downloads asynchronously the number of documents from each index of mongodb database.
     */
    private void setDBStats(String db, final ProgressBar progressBar)
    {
        if (progressBar != null) progressBar.setVisibility(View.VISIBLE);

        CountTask asyncTask1 = (CountTask) new CountTask(new AsyncResponse<Long>(){
            @Override
            public void processFinish(Long output){
                if (output != null) {

                    TextView textView = (TextView) findViewById(R.id.gridStat1_1);
                    textView.setText(output.toString());
                }
            }
        }).execute(db, "quasarset");
        CountTask asyncTask2 = (CountTask) new CountTask(new AsyncResponse<Long>(){
            @Override
            public void processFinish(Long output){
                if (output != null) {
                    TextView textView = (TextView) findViewById(R.id.gridStat2_1);
                    textView.setText(output.toString());
                }
            }
        }).execute(db, "quasar");
        CountTask asyncTask3 = (CountTask) new CountTask(new AsyncResponse<Long>(){
            @Override
            public void processFinish(Long output){
                if (output != null) {
                    TextView textView = (TextView) findViewById(R.id.gridStat3_1);
                    textView.setText(output.toString());
                }
            }
        }).execute(db, "pararesultset");
        CountTask asyncTask4 = (CountTask) new CountTask(new AsyncResponse<Long>(){
            @Override
            public void processFinish(Long output){
                if (output != null) {
                    TextView textView = (TextView) findViewById(R.id.gridStat4_1);
                    textView.setText(output.toString());
                }
                // Hide progress bar
                if (progressBar != null) progressBar.setVisibility(View.GONE);
            }
        }).execute(db, "pararesult");
    }

    /**
     * Returns network state
     * @return False on no connection or airplane mode. Otherwise true.
     */
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        readBundleSettings(savedInstanceState);

        initData();

        // Android api logger
        if (BuildConfig.DEBUG) { Log.d(this.getLocalClassName(), "onCreate called"); }

        Button button_data = (Button) findViewById(R.id.button_data);
        button_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //if (BuildConfig.DEBUG) { Log.d(Constants.LOG, "Innitializing data"); }
                initData();
            }
        });

        Button button_close = (Button) findViewById(R.id.button_close);
        button_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //if (BuildConfig.DEBUG) { Log.d(Constants.LOG, "Closing application"); }
                finish();
                System.exit(0);
            }
        });

        Button button_list = (Button) findViewById(R.id.button_list);
        button_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), QuasarSetListActivity.class);
                final int result = 1;
                startActivityForResult(intent, result);
            }
        });

        Button button_settings = (Button) findViewById(R.id.button_settings);
        button_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SettingsActivity.class);
                final int result = 1;
                startActivityForResult(intent, result);
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /**
     * This method will not be called but has to be present in application.
     * @param output async task result
     */
    @Override
    public void processFinish(Long output) {

    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        // ToDo: save state in this and other activities
        saveSettings(bundle);
        super.onSaveInstanceState(bundle);
    }

    /**
     * Stores current application state
     */
    private void saveSettings(Bundle bundle) {
        // ToDo: implement this method
        if (bundle != null){

        }
    }

    /**
     * Reads settings saved in bundle and overrides them with settings from preferences
     * @param savedInstanceState
     */
    private void readBundleSettings(Bundle savedInstanceState) {
        // ToDo: implement this method
        // ToDo: read state in this and other activities
        // 1. Make sure there is data to retrieve
        if(savedInstanceState != null){
            //String notes = savedInstanceState.getString("NOTES");
        }
        // 2. Retrieves the String stored in shared preferences or "EMPTY" if nothing
        String sPNotes = getPreferences(Context.MODE_PRIVATE).getString("NOTES", "EMPTY");
        if(!sPNotes.equals("EMPTY")){
            //notesEditText.setText(sPNotes);
        }
        // ToDo store read data in method's variables

        readSettings();
    }

    /**
     * Reads settings from preferences
     */
    private void readSettings() {
        // ToDo: read state in this and other activities ?
        SharedPreferences sp = getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE);
        SharedPreferences.Editor e = sp.edit();
        String url = sp.getString("url", "");
        if ( url == "") {
            url = "http://192.168.0.10:8080";
            this.urlQRS = url;
            e.putString("url", url);
            e.apply();
            if (BuildConfig.DEBUG) { Log.d(Constants.LOG, "QRS base url initialized to: " + url); }
            return;
        }
        e.apply();
        this.urlQRS = url;
        if (BuildConfig.DEBUG) { Log.d(Constants.LOG, "QRS base url set to: " + url); }

        // ToDo store past parametrizations in preferences for future analysis
        //SharedPreferences parametrizations = getSharedPreferences("quasarSpectrumParametrizations", Context.MODE_PRIVATE);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.kp.quasarspectrum/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.kp.quasarspectrum/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

}
