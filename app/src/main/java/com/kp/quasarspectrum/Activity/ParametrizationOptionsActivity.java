package com.kp.quasarspectrum.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.kp.quasarspectrum.Adapter.ParametrizationOptionsAdapter;
import com.kp.quasarspectrum.BuildConfig;
import com.kp.quasarspectrum.Interface.Constants;
import com.kp.quasarspectrum.Model.Command.ParametrizationOption;
import com.kp.quasarspectrum.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 07.06.16.
 */
public class ParametrizationOptionsActivity extends Activity {

    List<ParametrizationOption> parametrizationOptions = new ArrayList<ParametrizationOption>();
    ParametrizationOptionsAdapter lwAdapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametrizationoptions);

        // Innitialize parametrizationOptions list;
        InnitializeParameterizationOptionsList();

        // Using custom adapter
        lwAdapter = new ParametrizationOptionsAdapter(this, parametrizationOptions);

        // Using custom listView
        listView = (ListView) findViewById(R.id.parametrizationoptions_listview);

        // Applying the adapter
        listView.setAdapter(lwAdapter);

        // Save button controller
        Button parametrizationoptions_button_save = (Button) findViewById(R.id.parametrizationoptions_button_save);
        parametrizationoptions_button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SaveParametrizationOptions();
            }
        });

        // Reset button controller
        Button parametrizationoptions_button_reset = (Button) findViewById(R.id.parametrizationoptions_button_reset);
        parametrizationoptions_button_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ResetDialog();
                ResetParametrizationOptions();
                InnitializeParameterizationOptionsList();
                lwAdapter.notifyDataSetChanged();
                listView.invalidateViews();
            }
        });
    }



    private void SaveParametrizationOptions(){
        if (BuildConfig.DEBUG) Log.d(Constants.LOG, "Saving parametrization options to preferences");

        SharedPreferences sp = getSharedPreferences("quasarSpectrumParametrizationOptions", Context.MODE_PRIVATE);
        SharedPreferences.Editor e = sp.edit();
        e.clear();
        for(ParametrizationOption po : parametrizationOptions){
            //e.putString(po.getOption(), (po.getValue()=="")?null:po.getValue());
            e.putString(po.getOption(), po.getValue());
        }
        e.apply();

        SharedPreferences sp2 = getSharedPreferences("quasarSpectrumParametrizationOptionsEnabler", Context.MODE_PRIVATE);
        SharedPreferences.Editor e2 = sp2.edit();
        e2.clear();
        for(ParametrizationOption po : parametrizationOptions){
            e2.putBoolean(po.getOption(), po.getUsingDefault());
        }
        e2.apply();
    }

/*
    private void ResetDialog(){
        new AlertDialog.Builder(this)
                .setTitle("Parametrization options")
                .setMessage("Do you really want to reset parametrization options?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ResetParametrizationOptions();
                        Toast.makeText(ParametrizationOptionsActivity.this, "Reset completed", Toast.LENGTH_SHORT).show();
                    }})
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Toast.makeText(ParametrizationOptionsActivity.this, "Reset cancelled", Toast.LENGTH_SHORT).show();
                    }});
    }
*/

    private void ResetParametrizationOptions(){
        if (BuildConfig.DEBUG) Log.d(Constants.LOG, "Resetting parametrization options");

        // Clear preferences
        SharedPreferences sp = getSharedPreferences("quasarSpectrumParametrizationOptions", Context.MODE_PRIVATE);
        SharedPreferences.Editor e = sp.edit();
        e.clear();
        e.apply();

        // Clear enablers
        SharedPreferences sp2 = getSharedPreferences("quasarSpectrumParametrizationOptionsEnabler", Context.MODE_PRIVATE);
        SharedPreferences.Editor e2 = sp2.edit();
        e2.clear();
        e2.apply();

        // Clear parametrization options
        for(ParametrizationOption po : parametrizationOptions){
            po.setValue("");
        }
    }

    // ToDo: read 2nd parameter from string.xml
    private void InnitializeParameterizationOptionsList(){
        if (BuildConfig.DEBUG) Log.d(Constants.LOG, "Reading parametrization options from preferences");

        SharedPreferences sp = getSharedPreferences("quasarSpectrumParametrizationOptions", Context.MODE_PRIVATE);
        SharedPreferences sp2 = getSharedPreferences("quasarSpectrumParametrizationOptionsEnabler", Context.MODE_PRIVATE);

        parametrizationOptions.clear();

        // ToDo global use default !!!
        parametrizationOptions.add(new ParametrizationOption("useDefault", "All options", sp.getString("useDefault", ""), sp2.getBoolean("useDefault", true)));
        parametrizationOptions.add(new ParametrizationOption("spectralLines", "Spectral lines", "", sp2.getBoolean("spectralLines", true)));
        parametrizationOptions.add(new ParametrizationOption("continuumWindows", "Continuum windows", sp.getString("continuumWindows", ""), sp2.getBoolean("continuumWindows", true)));
        parametrizationOptions.add(new ParametrizationOption("ampwl", "Amp Wl", sp.getString("ampwl", ""), sp2.getBoolean("ampwl", true)));
        parametrizationOptions.add(new ParametrizationOption("feWindows", "Fw windows", sp.getString("feWindows", ""), sp2.getBoolean("feWindows", true)));
        parametrizationOptions.add(new ParametrizationOption("feTemp", "Fe temp", sp.getString("feTemp", ""), sp2.getBoolean("feTemp", true)));
        parametrizationOptions.add(new ParametrizationOption("feFwhmn", "Fe Fwhm", sp.getString("feFwhmn", ""), sp2.getBoolean("feFwhmn", true)));
        parametrizationOptions.add(new ParametrizationOption("FeFwhmt", "Fe Fwht", sp.getString("FeFwhmt", ""), sp2.getBoolean("FeFwhmt", true)));
        parametrizationOptions.add(new ParametrizationOption("feScaleRate", "Fe scale rate", sp.getString("feScaleRate", ""), sp2.getBoolean("feScaleRate", true)));
        parametrizationOptions.add(new ParametrizationOption("feFitRanges", "Fe fit ranges", sp.getString("feFitRanges", ""), sp2.getBoolean("feFitRanges", true)));
        parametrizationOptions.add(new ParametrizationOption("feFitType", "Fe fit type", sp.getString("feFitType", ""), sp2.getBoolean("feFitType", true)));
    }
}
