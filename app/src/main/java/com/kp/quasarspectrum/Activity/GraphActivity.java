package com.kp.quasarspectrum.Activity;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.kp.quasarspectrum.Misc.AxisFormatter;
import com.kp.quasarspectrum.Model.Quasar;
import com.kp.quasarspectrum.R;

import java.util.ArrayList;

public class GraphActivity extends Activity {

    private Typeface mTf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        // ToDo display errors as vertical bars
        // ToDo hide each bar's label

        // Getting quasar data
        Quasar quasar = (Quasar) getIntent().getSerializableExtra("quasar");

        // Displaying view widget
        LineChart lineChart = (LineChart) findViewById(R.id.graph_chart);


        // Setting parameters
        lineChart.setPinchZoom(false);
        lineChart.setDrawGridBackground(true);

        // Setting data and axis values
        ArrayList<Entry> valsY = new ArrayList<>();
        ArrayList<String> valsX = new ArrayList<>();
        for(int i = 0; i < quasar.getValues().length; i++){
            Entry entry = new Entry((float) quasar.getValues()[i], i);
            valsY.add(entry);
        }
        for(int i = 0; i < quasar.getValues().length; i++){
            valsX.add(Integer.toString(i));
        }
        LineDataSet set = new LineDataSet(valsY, quasar.getParams().getType() + ": " + quasar.getParams().getName());
        set.setDrawValues(false);
        set.setDrawCircles(false);

        LineData data = new LineData(valsX, set);
        data.setValueTextSize(10f);






        // Applying data
        lineChart.setData(data);

        // Customizing font
        //mTf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
        //data.setValueTypeface(mTf);


        // Formatting left and right axis labels
        AxisFormatter formatterLeft = new AxisFormatter();
        YAxis leftAxis = lineChart.getAxis(YAxis.AxisDependency.LEFT);
        leftAxis.setLabelCount(6, true);
        leftAxis.setValueFormatter(formatterLeft);
        AxisFormatter formatterRight = new AxisFormatter(true);
        YAxis rightAxis = lineChart.getAxis(YAxis.AxisDependency.RIGHT);
        rightAxis.setLabelCount(0, false);
        rightAxis.setValueFormatter(formatterRight);

        // Setting legend and description
        lineChart.setDescription("F(λ) Y-axis | λ X-axis");
        lineChart.setDescriptionTextSize(26);

        lineChart.notifyDataSetChanged();


        // Invalidate data just in case
        lineChart.invalidate();
    }


    /*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        // ToDo display errors as vertical bars
        // ToDo hide each bar's label

        // Getting quasar data
        Intent intent_main_activity = getIntent();
        Quasar quasar = (Quasar) getIntent().getSerializableExtra("quasar");

        // Displaying view widget
        BarChart barChart = (BarChart) findViewById(R.id.graph_chart);

        // Setting parameters
        barChart.setDrawBarShadow(false);
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(true);

        // Setting data and axis values
        ArrayList<BarEntry> valsY = new ArrayList<BarEntry>();
        ArrayList<String> valsX = new ArrayList<String>();
        for(int i = 0; i < quasar.getValues().length; i++){
            //float [] valueStack = new float[]{(float) quasar.getValues()[i] - (float) quasar.getError()[i], (float) quasar.getValues()[i], (float) quasar.getValues()[i] + (float) quasar.getError()[i]};
            //BarEntry entry = new BarEntry(valueStack, i, "Label " + i);
            BarEntry entry = new BarEntry((float) quasar.getValues()[i], i, "Label " + i);

            valsY.add(entry);
        }
        for(int i = 0; i < quasar.getValues().length; i++){
            valsX.add(Integer.toString(i));
        }
        BarDataSet set = new BarDataSet(valsY, quasar.getParams().getType() + ": " + quasar.getParams().getName());
        set.setBarSpacePercent(35f);
        ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
        dataSets.add(set);
        BarData data = new BarData(valsX, dataSets);
        data.setValueTextSize(10f);

        // Applying data
        barChart.setData(data);
        barChart.notifyDataSetChanged();
        //barChart.invalidate();

        // Customizing font
        //mTf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
        //data.setValueTypeface(mTf);

        // Formatting left and right axis labels
        AxisFormatter formatterLeft = new AxisFormatter();
        YAxis leftAxis = barChart.getAxis(YAxis.AxisDependency.LEFT);
        leftAxis.setLabelCount(6, true);
        leftAxis.setValueFormatter(formatterLeft);
        AxisFormatter formatterRight = new AxisFormatter(true);
        YAxis rightAxis = barChart.getAxis(YAxis.AxisDependency.RIGHT);
        rightAxis.setLabelCount(0, true);
        rightAxis.setValueFormatter(formatterRight);

        // Setting legend and description
        barChart.setDescription("F(λ) Y-axis | λ X-axis");
        barChart.setDescriptionTextSize(26);
        //barChart.setDescriptionPosition(10, 10);

        // Invalidate data just in case
        barChart.invalidate();
    }
    */
}
