package com.kp.quasarspectrum.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kp.quasarspectrum.Adapter.QuasarShortedAdapter;
import com.kp.quasarspectrum.BuildConfig;
import com.kp.quasarspectrum.Interface.AsyncResponse;
import com.kp.quasarspectrum.Interface.Constants;
import com.kp.quasarspectrum.Misc.RequestParams;
import com.kp.quasarspectrum.Model.QuasarShorted;
import com.kp.quasarspectrum.R;
import com.kp.quasarspectrum.Task.QRSTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by administrator on 05.06.16.
 */
public class QuasarShortedListActivity extends Activity implements AsyncResponse<Long>{

    public List<QuasarShorted> quasarShortedList = new ArrayList<QuasarShorted>();

    private int pageNr = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quasarshorted_list);

        final Intent intent = getIntent();
        final String quasar_set_oid = (String) getIntent().getStringExtra("quasar_set_oid");

        // Setting progress bar
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_spinner);

        // Using custom adapter
        final QuasarShortedAdapter lwAdapter = new QuasarShortedAdapter(this, quasarShortedList);

        // Using custom listView
        final ListView quasarlistView = (ListView) findViewById(R.id.quasarshorted_listView);

        // Applying the adapter
        //quasarlistView.setAdapter(lwAdapter);

        // Get the data
        getQuasarListBySetId(progressBar, quasar_set_oid, lwAdapter, quasarlistView, pageNr, GetPageSize());

        // Setting on click action. Should direct to quasar details.
        quasarlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(view.getContext(), QuasarDetailsActivity.class);
                final int result = 1;
                QuasarShorted obj = (QuasarShorted) adapterView.getItemAtPosition(i);
                intent.putExtra("id", obj.getId());
                startActivityForResult(intent, result);
            }}
        );

        // Parametrization menu button
        Button button_parametrizationmenu = (Button) findViewById(R.id.button_parametrizationmenu);
        button_parametrizationmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (quasar_set_oid != null){
                    Intent intent = new Intent(view.getContext(), ParametrizationMenuActivity.class);
                    final int result = 1;
                    intent.putExtra("quasar_set_oid", quasar_set_oid);
                    startActivityForResult(intent, result);
                }
            }
        });

        // Previous button
        Button button_quasarshortedlist_previous = (Button) findViewById(R.id.button_quasarshortedlist_previous);
        button_quasarshortedlist_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((pageNr - 1) < 0) pageNr = 0;
                    else pageNr--;
                Toast.makeText(QuasarShortedListActivity.this, "Page: " + (pageNr + 1), Toast.LENGTH_SHORT).show();
                if (BuildConfig.DEBUG) {
                    Log.d(Constants.LOG, "Getting previous page = " + pageNr + ", size = " + GetPageSize());
                }
                getQuasarListBySetId(progressBar, quasar_set_oid, lwAdapter, quasarlistView, pageNr, GetPageSize());
            }
        });

        // Next button
        Button button_quasarshortedlist_next = (Button) findViewById(R.id.button_quasarshortedlist_next);
        button_quasarshortedlist_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pageNr++;
                Toast.makeText(QuasarShortedListActivity.this, "Page: " + (pageNr + 1), Toast.LENGTH_SHORT).show();
                if (BuildConfig.DEBUG) {
                    Log.d(Constants.LOG, "Getting next page = " + pageNr + ", size = " + GetPageSize());
                }
                getQuasarListBySetId(progressBar, quasar_set_oid, lwAdapter, quasarlistView, pageNr, GetPageSize());
            }
        });
    }

    private void getQuasarListBySetId(ProgressBar progressBar, final String quasar_set_oid, final QuasarShortedAdapter adapter, final ListView listView, Integer pageNr, Integer count){
        SharedPreferences sp = getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE);
        String urlQRS = sp.getString("url", "");
        RequestParams rp = new RequestParams(urlQRS, "quasarshorted", "bysetid", new HashMap<String, String>() {{
            put("id", quasar_set_oid);
        }}, pageNr, count);
        taskQuasarList(progressBar, rp, adapter, listView);
    }

    private void taskQuasarList(final ProgressBar progressBar, RequestParams rp, final QuasarShortedAdapter adapter, final ListView listView){
        if (progressBar != null) progressBar.setVisibility(View.VISIBLE);
        new QRSTask<List<QuasarShorted>>(new AsyncResponse<List<QuasarShorted>>(){
            @Override
            public void processFinish(List<QuasarShorted> output){
                if (output != null) {
                    quasarShortedList.clear();
                    quasarShortedList.addAll(output);
                    listView.setAdapter(adapter);
                    if (BuildConfig.DEBUG) {
                        Log.d(Constants.LOG, "AsyncResponse returned size = " + output.size());
                    }
                }
                if (progressBar != null) progressBar.setVisibility(View.GONE);
            }
        }, rp).execute();
    }

    private Integer GetPageSize(){
        return getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE).getInt("pagesize", 100);
    }

    /**
     * This method will not be called but has to be present in application.
     * @param output async task result
     */
    @Override
    public void processFinish(Long output) {

    }
}
