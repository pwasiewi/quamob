package com.kp.quasarspectrum.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kp.quasarspectrum.BuildConfig;
import com.kp.quasarspectrum.Interface.AsyncResponse;
import com.kp.quasarspectrum.Interface.Constants;
import com.kp.quasarspectrum.Misc.RequestParams;
import com.kp.quasarspectrum.Model.Quasar;
import com.kp.quasarspectrum.R;
import com.kp.quasarspectrum.Task.QRSTask;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by administrator on 05.06.16.
 */
public class QuasarDetailsActivity extends Activity implements AsyncResponse<Long>{

    private Quasar quasar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quasar_details);

        Intent intent = getIntent();
        String id = (String) getIntent().getStringExtra("id");
        if (BuildConfig.DEBUG) {
            Log.d(Constants.LOG, "Displaying details of quasar: " + id);
        }

        // Setting progress bar
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_spinner);

        // Getting Quasar details (full version of QuasarShorted)
        getQuasarById(progressBar, id);

        // Show spectrum graph
        Button button_quasar_details_graph = (Button) findViewById(R.id.button_quasar_details_graph);
        button_quasar_details_graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (quasar != null){
                    Intent intent = new Intent(view.getContext(), GraphActivity.class);
                    final int result = 1;
                    intent.putExtra("quasar", (Serializable) quasar);   // Serializable
                    startActivityForResult(intent, result);
                }
            }
        });

        // Parametrization menu
        Button button_quasar_details_parametrizationmenu = (Button) findViewById(R.id.button_quasar_details_parametrizationmenu);
        button_quasar_details_parametrizationmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (quasar != null){
                    Intent intent = new Intent(view.getContext(), ParametrizationMenuActivity.class);
                    final int result = 1;
                    intent.putExtra("quasar_set_oid", quasar.getQuasarSetOid());
                    startActivityForResult(intent, result);
                }
            }
        });
    }

    private void getQuasarById(ProgressBar progressBar, final String id){
        SharedPreferences sp = getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE);
        String urlQRS = sp.getString("url", "");
        RequestParams rp = new RequestParams(urlQRS, "quasar", "byid", new HashMap<String, String>() {{
            put("id", id);
        }});
        taskQuasar(progressBar, rp);
    }

    private void taskQuasar(final ProgressBar progressBar, RequestParams rp){
        if (progressBar != null) progressBar.setVisibility(View.VISIBLE);
        new QRSTask<Quasar>(new AsyncResponse<Quasar>(){
            @Override
            public void processFinish(Quasar output){
                if (output != null) {
                    quasar = output;
                    if (BuildConfig.DEBUG) {
                        Log.d(Constants.LOG, "AsyncResponse returned object: " + output.toString());
                    }

                    // Updating data on layout
                    TextView textView0 = (TextView) findViewById(R.id.quasar_details_value_id);
                    textView0.setText(quasar.getId());

                    TextView textView1 = (TextView) findViewById(R.id.quasar_details_value_setid);
                    textView1.setText(quasar.getQuasarSetOid());

                    TextView textView2 = (TextView) findViewById(R.id.quasar_details_value_name);
                    textView2.setText(quasar.getParams().getName());

                    TextView textView3 = (TextView) findViewById(R.id.quasar_details_value_type);
                    textView3.setText(quasar.getParams().getType());

                    TextView textView4 = (TextView) findViewById(R.id.quasar_details_value_z);
                    textView4.setText(String.format("%f", quasar.getParams().getZ()));

                    TextView textView5 = (TextView) findViewById(R.id.quasar_details_value_ra);
                    textView5.setText(String.format("%f", quasar.getParams().getRa()));

                    TextView textView6 = (TextView) findViewById(R.id.quasar_details_value_dec);
                    textView6.setText(String.format("%f", quasar.getParams().getDec()));

                    TextView textView7 = (TextView) findViewById(R.id.quasar_details_value_mjd);
                    textView7.setText(String.format("%d", quasar.getParams().getMjd()));

                    TextView textView8 = (TextView) findViewById(R.id.quasar_details_value_plate);
                    textView8.setText(String.format("%d", quasar.getParams().getPlate()));

                    TextView textView9 = (TextView) findViewById(R.id.quasar_details_value_fiber);
                    textView9.setText(String.format("%d", quasar.getParams().getFiber()));

                    TextView textView10 = (TextView) findViewById(R.id.quasar_details_value_a);
                    textView10.setText(String.format("%f", quasar.getParams().getA()));

                    TextView textView11 = (TextView) findViewById(R.id.quasar_details_value_b);
                    textView11.setText(String.format("%f", quasar.getParams().getB()));

                    TextView textView12 = (TextView) findViewById(R.id.quasar_details_value_size);
                    textView12.setText(String.format("%d", quasar.getSize()));
                }
                if (progressBar != null) progressBar.setVisibility(View.GONE);
            }
        }, rp).execute();
    }

    // ToDo implement this method in all db reliant tasks
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED;
    }

    /**
     * This method will not be called but has to be present in application.
     * @param output async task result
     */
    @Override
    public void processFinish(Long output) {

    }
}
