package com.kp.quasarspectrum.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kp.quasarspectrum.Adapter.ParaResultAdapter;
import com.kp.quasarspectrum.BuildConfig;
import com.kp.quasarspectrum.Interface.AsyncResponse;
import com.kp.quasarspectrum.Interface.Constants;
import com.kp.quasarspectrum.Misc.RequestParams;
import com.kp.quasarspectrum.Model.ParaResult;
import com.kp.quasarspectrum.R;
import com.kp.quasarspectrum.Task.QRSTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by administrator on 09.06.16.
 */
public class ParaResultListActivity extends Activity implements AsyncResponse<Long>{

    public List<ParaResult> paraResultList = new ArrayList<ParaResult>();

    private int pageNr = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pararesult);

        final String para_result_set_oid = getIntent().getStringExtra("para_result_set_oid");

        // Setting progress bar
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_spinner);

        // Using custom adapter
        final ParaResultAdapter lwAdapter = new ParaResultAdapter(this, paraResultList);

        // Using custom listView
        final ListView listView = (ListView) findViewById(R.id.pararesult_listView);

        // Applying the adapter
        //listView.setAdapter(lwAdapter);

        // Get the data
        getParaResultListBySetId(progressBar, para_result_set_oid, lwAdapter, listView, pageNr, GetPageSize());

        // Setting on click action. Should direct to ParaResult details
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(view.getContext(), ParaResultDetailsActivity.class);
                final int result = 1;
                ParaResult obj = (ParaResult) adapterView.getItemAtPosition(i);
                intent.putExtra("id", obj.getId());
                startActivityForResult(intent, result);
            }}
        );

        // Previous button
        Button button_pararesult_previous = (Button) findViewById(R.id.button_pararesult_previous);
        button_pararesult_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((pageNr - 1) < 0) pageNr = 0;
                    else pageNr--;
                Toast.makeText(ParaResultListActivity.this, "Page: " + (pageNr + 1), Toast.LENGTH_SHORT).show();
                if (BuildConfig.DEBUG) {
                    Log.d(Constants.LOG, "Getting previous page = " + pageNr + ", size = " + GetPageSize());
                }
                getParaResultListBySetId(progressBar, para_result_set_oid, lwAdapter, listView, pageNr, GetPageSize());
            }
        });

        // Next button
        Button button_pararesult_next = (Button) findViewById(R.id.button_pararesult_next);
        button_pararesult_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pageNr++;
                Toast.makeText(ParaResultListActivity.this, "Page: " + (pageNr + 1), Toast.LENGTH_SHORT).show();
                if (BuildConfig.DEBUG) {
                    Log.d(Constants.LOG, "Getting next page = " + pageNr + ", size = " + GetPageSize());
                }
                getParaResultListBySetId(progressBar, para_result_set_oid, lwAdapter, listView, pageNr, GetPageSize());
            }
        });
    }

    private void getParaResultListBySetId(ProgressBar progressBar, final String para_result_set_oid, final ParaResultAdapter adapter, final ListView listView, Integer start, Integer pageSize){
        SharedPreferences sp = getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE);
        String urlQRS = sp.getString("url", "");
        RequestParams rp = new RequestParams(urlQRS, "pararesult", "bysetid", new HashMap<String, String>() {{
            put("id", para_result_set_oid);
        }}, start, pageSize);
        taskParaResultList(progressBar, rp, adapter, listView);
    }

    private void taskParaResultList(final ProgressBar progressBar, RequestParams rp, final ParaResultAdapter adapter, final ListView listView){
        if (progressBar != null) progressBar.setVisibility(View.VISIBLE);
        new QRSTask<List<ParaResult>>(new AsyncResponse<List<ParaResult>>(){
            @Override
            public void processFinish(List<ParaResult> output){
                if (output != null) {
                    paraResultList.clear();
                    paraResultList.addAll(output);
                    listView.setAdapter(adapter);
                    if (BuildConfig.DEBUG) {
                        Log.d(Constants.LOG, "AsyncResponse returned size = " + output.size());
                    }
                }
                if (progressBar != null) progressBar.setVisibility(View.GONE);
            }
        }, rp).execute();
    }

    private Integer GetPageSize(){
        return getSharedPreferences("quasarSpectrum", Context.MODE_PRIVATE).getInt("pagesize", 100);
    }

    /**
     * This method will not be called but has to be present in application.
     * @param output async task result
     */
    @Override
    public void processFinish(Long output) {

    }
}
