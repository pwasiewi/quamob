package com.kp.quasarspectrum.Interface;

/**
 * Created by administrator on 27.05.16.
 */
public interface AsyncResponse <T> {
    void processFinish(T output);
}