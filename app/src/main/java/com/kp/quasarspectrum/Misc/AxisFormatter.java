package com.kp.quasarspectrum.Misc;

import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;

import java.text.DecimalFormat;

/**
 * Created by administrator on 06.06.16.
 */
public class AxisFormatter implements YAxisValueFormatter {

    private DecimalFormat mFormat;
    private Boolean empty = false;


    public AxisFormatter () {
        this(false);
    }

    public AxisFormatter (Boolean empty) {
        this.empty = empty;
        mFormat = new DecimalFormat("###,###,##0.0"); // use one decimal
    }

    @Override
    public String getFormattedValue(float value, YAxis yAxis) {
        // write your logic here
        // access the YAxis object to get more information
        //return mFormat.format(value) + " $"; // e.g. append a dollar-sign

        if (empty)
            return "";

        return String.format("%s", value);

        //return String.format("%.1f", value);
    }
}