package com.kp.quasarspectrum.Misc;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 01.06.16.
 */
public class RequestParams {

    private String url;             // Final url
    private String urlBase;         // ToDo: read get url in contructor?
    private String className;       // ToDo: change to class type?
    private String method;
    private Integer start;
    private Integer count;
    private Boolean isList = false;
    private Map<String, String> parameters = new HashMap<String, String>();

    //public RequestParams() {}

    public RequestParams(String urlBase, String className, String method, Map<String, String> parameters, Integer start, Integer count) {
        this.urlBase = urlBase;
        this.className = className;
        this.method = method;
        this.start = start;
        this.parameters = parameters;
        this.count = count;
        if (count != 0) isList = true;
        PrepareUrl();
    }

    public RequestParams(String urlBase, String className) {
        this(urlBase, className, null, null, 0, Integer.MAX_VALUE);
    }

    public RequestParams(String urlBase, String className, Integer start, Integer count) {
        this(urlBase, className, null, null, start, count );
    }

    public RequestParams(String urlBase, String className, String method, Map<String, String> parameters) {
        this(urlBase, className, method, parameters, 0, 0);
    }

    private void PrepareUrl(){
        url = urlBase;

        if (!urlBase.endsWith("/"))
            url += "/";

        url += "get/" + className;

        if (count != 0)
            url += "/page/" + start.toString() + "/" + count.toString();

        if (method != null){
            url += "/" + method + "?";

            for(Map.Entry<String, String> entry : parameters.entrySet()) {
                url += entry.getKey() + "=" + entry.getValue() + "&";
            }

            if (url.endsWith("&"))
                url = url.substring(0, url.length() - 1);
        }

        //return url;
    }

    public String getUrl() {
        return url;
    }

    public String getMethod() {
        return method;
    }

    public String getClassName() {
        return className;
    }

    public Integer getStart() {
        return start;
    }

    public Integer getCount() {
        return count;
    }

    public Boolean getIsList() {
        return isList;
    }
}
