package com.kp.quasarspectrum.Misc;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Returns network state
 * @return False on no connection or airplane mode. Otherwise true.
 */
public final class Network {
    private Network() {}

    public static Boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED;
    }

}