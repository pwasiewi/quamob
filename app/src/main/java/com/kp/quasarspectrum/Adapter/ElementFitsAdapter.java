package com.kp.quasarspectrum.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kp.quasarspectrum.Model.Property.ElementFits;
import com.kp.quasarspectrum.R;

import java.util.List;

/**
 * Created by administrator on 09.06.16.
 */
public class ElementFitsAdapter extends ArrayAdapter<ElementFits> {

    public ElementFitsAdapter (Context context, List<ElementFits> objList) {
        super(context, R.layout.rowlayout_elementfits, objList);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        // Layout inflater
        LayoutInflater inflater = LayoutInflater.from(getContext());

        // List view row
        View view = inflater.inflate(R.layout.rowlayout_elementfits, parent, false);

        // Row entry
        ElementFits item = getItem(position);

        // Set data to textview
        TextView textView1 = (TextView) view.findViewById(R.id.rowlayout_elementfits_textview1);
        //
        textView1.setText(item.toString());

        return view;
    }
}
