package com.kp.quasarspectrum.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.kp.quasarspectrum.Model.Command.ParametrizationOption;
import com.kp.quasarspectrum.R;

import java.util.List;

/**
 * Created by administrator on 10.06.16.
 */
public class ParametrizationOptionsAdapter extends ArrayAdapter<ParametrizationOption> {

    public ParametrizationOptionsAdapter (Context context, List<ParametrizationOption> objList) {
        super(context, R.layout.rowlayout_parametrizationoptions, objList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        // Layout inflater
        LayoutInflater inflater = LayoutInflater.from(getContext());

        // List view row
        View view = inflater.inflate(R.layout.rowlayout_parametrizationoptions, parent, false);

        // Row entry
        final ParametrizationOption item = getItem(position);

        // Set data to textview
        TextView textView1 = (TextView) view.findViewById(R.id.rowlayout_parametrizationoptions_textview_name);
        textView1.setText(item.getOptionString());

        // Get Edittext
        final EditText editText = (EditText) view.findViewById(R.id.rowlayout_parametrizationoptions_edittext);
        editText.setVisibility(item.getUsingDefault() ? View.INVISIBLE : View.VISIBLE);
        editText.setText(item.getValue());

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                item.setValue(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        // Radiobutton Yes click controller
        RadioButton RBYes = (RadioButton) view.findViewById(R.id.rowlayout_parametrizationoptions_radiobutton_yes);
        RBYes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                item.setUsingDefault(true);
                editText.setVisibility(View.INVISIBLE);
            }
        });
        RBYes.setChecked(item.getUsingDefault());

        // Radiobutton No click controller
        RadioButton RBNo = (RadioButton) view.findViewById(R.id.rowlayout_parametrizationoptions_radiobutton_no);
        RBNo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                item.setUsingDefault(false);
                editText.setVisibility(View.VISIBLE);
            }
        });
        RBNo.setChecked(!item.getUsingDefault());


        //if (BuildConfig.DEBUG) Log.d(Constants.LOG, item.getOptionString());

        return view;
    }

}
