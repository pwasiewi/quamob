package com.kp.quasarspectrum.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kp.quasarspectrum.Model.QuasarShorted;
import com.kp.quasarspectrum.R;

import java.util.List;

/**
 * Created by administrator on 03.06.16.
 */
public class QuasarShortedAdapter extends ArrayAdapter<QuasarShorted> {

    public QuasarShortedAdapter(Context context, List<QuasarShorted> objList) {
        super(context, R.layout.rowlayout_quasarshorted, objList);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        // Layout inflater
        LayoutInflater inflater = LayoutInflater.from(getContext());

        // List view row
        View view = inflater.inflate(R.layout.rowlayout_quasarshorted, parent, false);

        // Row entry
        QuasarShorted q = getItem(position);

        // Set data to textview
        TextView textView1 = (TextView) view.findViewById(R.id.rowlayout_quasar_textview1);
        //
        textView1.setText(q.toString());

        return view;
    }
}
