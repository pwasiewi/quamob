package com.kp.quasarspectrum.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kp.quasarspectrum.Model.QuasarSet;
import com.kp.quasarspectrum.R;

import java.util.List;

/**
 * Created by administrator on 03.06.16.
 */
public class QuasarSetAdapter extends ArrayAdapter<QuasarSet> {

    public QuasarSetAdapter(Context context, List<QuasarSet> objList) {
        super(context, R.layout.rowlayout_quasarset, objList);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        // Layout inflater
        LayoutInflater inflater = LayoutInflater.from(getContext());

        // List view row
        View view = inflater.inflate(R.layout.rowlayout_quasarset, parent, false);

        // Row entry
        QuasarSet qs = getItem(position);

        // Set data to textview
        TextView textView1 = (TextView) view.findViewById(R.id.rowlayout_quasarset_textview1);
        //textView1.setText("Name: " + qs.getName() + ", date: " + qs.getDate() + ", insert date " + qs.getInsertDate() + ", id: " + qs.getId());
        textView1.setText(qs.toString());

        return view;
    }
}
