package com.kp.quasarspectrum.Task;

import android.os.AsyncTask;
import android.util.Log;

import com.kp.quasarspectrum.BuildConfig;
import com.kp.quasarspectrum.Interface.AsyncResponse;
import com.kp.quasarspectrum.Interface.Constants;
import com.kp.quasarspectrum.Misc.RequestParams;
import com.kp.quasarspectrum.Model.ParaResult;
import com.kp.quasarspectrum.Model.ParaResultSet;
import com.kp.quasarspectrum.Model.Quasar;
import com.kp.quasarspectrum.Model.QuasarSet;
import com.kp.quasarspectrum.Model.QuasarShorted;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by administrator on 29.05.16.
 */
public class QRSTask<T> extends AsyncTask<Void, Void, T> {
    public AsyncResponse<T> delegate = null;
    private RequestParams requestParams;

    public QRSTask(AsyncResponse<T> delegate, RequestParams requestParams) {
        this.delegate = delegate;
        this.requestParams = requestParams;
    }

    @Override
    protected void onPostExecute(T result) {
        //super.onPostExecute(result);
        //if (BuildConfig.DEBUG) { Log.i(Constants.LOG, "Result = " + result.toString()); }

        // Test result for being a list (and type) and logging it's size?
        delegate.processFinish(result);
    }

    @Override
    protected T doInBackground(Void... voids) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        //Log.wtf(Constants.LOG, String.format("Time = %d", Calendar.getInstance().getTimeInMillis()));
        Random rand = new Random();
        Long transaction = rand.nextLong();

        if (BuildConfig.DEBUG) {
            Log.i(Constants.LOG, "TID:" + transaction + "| Querying address: " + requestParams.getUrl());
        }
        //if (isCancelled()) break;

        try {
            // ToDo in async mode
            // https://stackoverflow.com/questions/6343166/how-to-fix-android-os-networkonmainthreadexception

            if (requestParams.getIsList()) {
                if (requestParams.getClassName() == "quasarset"){
                    ResponseEntity<QuasarSet[]> responseEntity = restTemplate.getForEntity(requestParams.getUrl(), QuasarSet[].class);
                    if (responseEntity != null) {
                        T retList = (T) new ArrayList<QuasarSet>(Arrays.asList(responseEntity.getBody()));
                        Log.i(Constants.LOG, "TID:" + transaction + "| Async list task returned with status code: " + String.valueOf(responseEntity.getStatusCode()) + " - " + responseEntity.getStatusCode().name());
                        return retList;
                    }
                    else {
                        Log.e(Constants.LOG, "TID:" + transaction + "| Async list task returned null");
                        return null;
                    }
                }
                if (requestParams.getClassName() == "quasar"){
                    ResponseEntity<Quasar[]> responseEntity = restTemplate.getForEntity(requestParams.getUrl(), Quasar[].class);
                    if (responseEntity != null) {
                        T retList = (T) new ArrayList<Quasar>(Arrays.asList(responseEntity.getBody()));
                        Log.i(Constants.LOG, "TID:" + transaction + "| Async list task returned with status code: " + String.valueOf(responseEntity.getStatusCode()) + " - " + responseEntity.getStatusCode().name());
                        return retList;
                    }
                    else {
                        Log.e(Constants.LOG, "TID:" + transaction + "| Async list task returned null");
                        return null;
                    }
                }
                if (requestParams.getClassName() == "quasarshorted"){
                    ResponseEntity<QuasarShorted[]> responseEntity = restTemplate.getForEntity(requestParams.getUrl(), QuasarShorted[].class);
                    if (responseEntity != null) {
                        T retList = (T) new ArrayList<QuasarShorted>(Arrays.asList(responseEntity.getBody()));
                        Log.i(Constants.LOG, "TID:" + transaction + "| Async list task returned with status code: " + String.valueOf(responseEntity.getStatusCode()) + " - " + responseEntity.getStatusCode().name());
                        return retList;
                    }
                    else {
                        Log.e(Constants.LOG, "TID:" + transaction + "| Async list task returned null");
                        return null;
                    }
                }
                if (requestParams.getClassName() == "pararesult"){
                    ResponseEntity<ParaResult[]> responseEntity = restTemplate.getForEntity(requestParams.getUrl(), ParaResult[].class);
                    if (responseEntity != null) {
                        T retList = (T) new ArrayList<ParaResult>(Arrays.asList(responseEntity.getBody()));
                        Log.i(Constants.LOG, "TID:" + transaction + "| Async list task returned with status code: " + String.valueOf(responseEntity.getStatusCode()) + " - " + responseEntity.getStatusCode().name());
                        return retList;
                    }
                    else {
                        Log.e(Constants.LOG, "TID:" + transaction + "| Async list task returned null");
                        return null;
                    }
                }
                if (requestParams.getClassName() == "pararesultset"){
                    ResponseEntity<ParaResultSet[]> responseEntity = restTemplate.getForEntity(requestParams.getUrl(), ParaResultSet[].class);
                    if (responseEntity != null) {
                        T retList = (T) new ArrayList<ParaResultSet>(Arrays.asList(responseEntity.getBody()));
                        Log.i(Constants.LOG, "TID:" + transaction + "| Async list task returned with status code: " + String.valueOf(responseEntity.getStatusCode()) + " - " + responseEntity.getStatusCode().name());
                        return retList;
                    }
                    else {
                        Log.e(Constants.LOG, "TID:" + transaction + "| Async list task returned null");
                        return null;
                    }
                }
                else
                    return null;
            }
            else {
                if (requestParams.getClassName() == "quasarset"){
                    ResponseEntity<QuasarSet> responseEntity = restTemplate.getForEntity(requestParams.getUrl(), QuasarSet.class);
                    if (responseEntity != null) {
                        T retObj = (T)responseEntity.getBody();
                        Log.i(Constants.LOG, "TID:" + transaction + "| Async object task returned with status code: " + String.valueOf(responseEntity.getStatusCode()) + " - " + responseEntity.getStatusCode().name());
                        return retObj;
                    }
                    else {
                        Log.e(Constants.LOG, "TID:" + transaction + "| Async object task returned null");
                        return null;
                    }
                }
                if (requestParams.getClassName() == "quasar"){
                    ResponseEntity<Quasar> responseEntity = restTemplate.getForEntity(requestParams.getUrl(), Quasar.class);
                    if (responseEntity != null) {
                        T retObj = (T)responseEntity.getBody();
                        Log.i(Constants.LOG, "TID:" + transaction + "| Async object task returned with status code: " + String.valueOf(responseEntity.getStatusCode()) + " - " + responseEntity.getStatusCode().name());
                        return retObj;
                    }
                    else {
                        Log.e(Constants.LOG, "TID:" + transaction + "| Async object task returned null");
                        return null;
                    }
                }
                if (requestParams.getClassName() == "quasarshorted"){
                    ResponseEntity<QuasarShorted> responseEntity = restTemplate.getForEntity(requestParams.getUrl(), QuasarShorted.class);
                    if (responseEntity != null) {
                        T retObj = (T)responseEntity.getBody();
                        Log.i(Constants.LOG, "TID:" + transaction + "| Async object task returned with status code: " + String.valueOf(responseEntity.getStatusCode()) + " - " + responseEntity.getStatusCode().name());
                        return retObj;
                    }
                    else {
                        Log.e(Constants.LOG, "TID:" + transaction + "| Async object task returned null");
                        return null;
                    }
                }
                if (requestParams.getClassName() == "pararesult"){
                    ResponseEntity<ParaResult> responseEntity = restTemplate.getForEntity(requestParams.getUrl(), ParaResult.class);
                    if (responseEntity != null) {
                        T retObj = (T)responseEntity.getBody();
                        Log.i(Constants.LOG, "TID:" + transaction + "| Async object task returned with status code: " + String.valueOf(responseEntity.getStatusCode()) + " - " + responseEntity.getStatusCode().name());
                        return retObj;
                    }
                    else {
                        Log.e(Constants.LOG, "TID:" + transaction + "| Async object task returned null");
                        return null;
                    }
                }
                if (requestParams.getClassName() == "pararesultshorted"){
                    ResponseEntity<ParaResultSet> responseEntity = restTemplate.getForEntity(requestParams.getUrl(), ParaResultSet.class);
                    if (responseEntity != null) {
                        T retObj = (T)responseEntity.getBody();
                        Log.i(Constants.LOG, "TID:" + transaction + "| Async object task returned with status code: " + String.valueOf(responseEntity.getStatusCode()) + " - " + responseEntity.getStatusCode().name());
                        return retObj;
                    }
                    else {
                        Log.e(Constants.LOG, "TID:" + transaction + "| Async object task returned null");
                        return null;
                    }
                }
                else
                    return null;
            }

            //if (BuildConfig.DEBUG) {
            //    Log.i(Constants.LOG, t.toString());
            //}

        } catch (RestClientException e) {
            //HandleNetworkError(e);
            if (BuildConfig.DEBUG) {
                Log.d(Constants.LOG, e.toString());
            }
            //e.printStackTrace();
            return null;
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.e(Constants.LOG, e.toString());
            }
            return null;
        }
    }

    // ToDo
    private void HandleNetworkError(RestClientException e) {

    }

    protected void onProgressUpdate(Integer... progress) {
        //setProgressPercent(progress[0]);
        if (BuildConfig.DEBUG) {
            Log.d(Constants.LOG, "Progress: " + progress[0] + "%");
        }
    }
}