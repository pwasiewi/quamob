package com.kp.quasarspectrum.Task;

import android.os.AsyncTask;
import android.util.Log;

import com.kp.quasarspectrum.BuildConfig;
import com.kp.quasarspectrum.Interface.AsyncResponse;
import com.kp.quasarspectrum.Interface.Constants;
import com.kp.quasarspectrum.Model.QuasarSet;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Created by administrator on 26.05.16.
 */
public class CountTask extends AsyncTask<String, Void, Long> {

    public AsyncResponse<Long> delegate = null;

    public CountTask(AsyncResponse<Long> delegate){
        this.delegate=delegate;
    }

    @Override
    protected void onPostExecute(Long result) {
        //super.onPostExecute(result);
        if (BuildConfig.DEBUG) { Log.i(Constants.LOG, "Result = " + result.toString()); }
        delegate.processFinish(result);
    }


    private Exception exception;

    //onPreExecute()

    @Override
    protected Long doInBackground(String... strs) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        Long resp;
        //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String url = strs[0] + "/get/" + strs[1] + "/count";
        if (BuildConfig.DEBUG) { Log.i("Querying address: ", url); }
        //if (isCancelled()) break;

        try {
            // ToDo in async mode
            // https://stackoverflow.com/questions/6343166/how-to-fix-android-os-networkonmainthreadexception
            resp = restTemplate.getForObject(url, Long.class);
        } catch (RestClientException e) {
            HandleNetworkError(e);
            if (BuildConfig.DEBUG) { Log.d(Constants.LOG, e.toString()); }
            //e.printStackTrace();
            return new Long(-1);
        }
        catch (Exception e){
            //if ()
            if (BuildConfig.DEBUG) { Log.e(Constants.LOG, e.toString()); }
            return new Long(-1);
        }
        return resp;
    }

    private void HandleNetworkError(RestClientException e) {
    }

    protected void onProgressUpdate(Integer... progress) {
        //setProgressPercent(progress[0]);
        if (BuildConfig.DEBUG) { Log.d(Constants.LOG, "Progress: " + progress[0] + "%"); }
    }


    /**
     * Created by administrator on 29.05.16.
     */
    public static class GetQuasarSetById extends AsyncTask<String, Void, QuasarSet> {

        public AsyncResponse<QuasarSet> delegate = null;

        public GetQuasarSetById(AsyncResponse<QuasarSet> delegate){
            this.delegate=delegate;
        }


        @Override
        protected void onPostExecute(QuasarSet result) {
            //super.onPostExecute(result);
            if (BuildConfig.DEBUG) { Log.i(Constants.LOG, "Result = " + result.toString()); }
            delegate.processFinish(result);
        }


        private Exception exception;

        //onPreExecute()

        @Override
        protected QuasarSet doInBackground(String... strs) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            QuasarSet resp;
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            String url = strs[0];
            if (BuildConfig.DEBUG) { Log.i("Querying address: ", url); }
            //if (isCancelled()) break;

            try {
                // ToDo in async mode
                // https://stackoverflow.com/questions/6343166/how-to-fix-android-os-networkonmainthreadexception
                resp = restTemplate.getForObject(url, QuasarSet.class);
            } catch (RestClientException e) {
                HandleNetworkError(e);
                if (BuildConfig.DEBUG) { Log.d(Constants.LOG, e.toString()); }
                //e.printStackTrace();
                return null;
            }
            catch (Exception e){
                //if ()
                if (BuildConfig.DEBUG) { Log.e(Constants.LOG, e.toString()); }
                return null;
            }
            return resp;
        }

        private void HandleNetworkError(RestClientException e) {
        }

        protected void onProgressUpdate(Integer... progress) {
            //setProgressPercent(progress[0]);
            if (BuildConfig.DEBUG) { Log.d(Constants.LOG, "Progress: " + progress[0] + "%"); }
        }


    }
}