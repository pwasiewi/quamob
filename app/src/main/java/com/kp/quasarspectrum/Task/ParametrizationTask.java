package com.kp.quasarspectrum.Task;

import android.os.AsyncTask;
import android.util.Log;

import com.kp.quasarspectrum.BuildConfig;
import com.kp.quasarspectrum.Interface.AsyncResponse;
import com.kp.quasarspectrum.Interface.Constants;
import com.kp.quasarspectrum.Model.Command.Command;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

/**
 * Created by administrator on 12.06.16.
 */
public class ParametrizationTask extends AsyncTask<Void, Void, String> {
    public AsyncResponse delegate = null;
    private Command command;
    private String url;

    public ParametrizationTask(AsyncResponse delegate, Command command, String url) {
        this.delegate = delegate;
        this.command = command;
        this.url = url;
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }

    @Override
    protected String doInBackground(Void... voids) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        //Log.wtf(Constants.LOG, String.format("Time = %d", Calendar.getInstance().getTimeInMillis()));
        Random rand = new Random();
        Long transaction = rand.nextLong();

        if (BuildConfig.DEBUG) {
            Log.i(Constants.LOG, "TID:" + transaction + "| Querying address: " + url);
            Log.d(Constants.LOG, "TID:" + transaction + "| " + command.toString());
        }
        //if (isCancelled()) break;

        try {
            // ToDo in async mode
            // https://stackoverflow.com/questions/6343166/how-to-fix-android-os-networkonmainthreadexception

                ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, command, String.class);

                if (responseEntity != null) {
                    String responceBody = (String) responseEntity.getBody();
                    Log.i(Constants.LOG, "TID:" + transaction + "| Async parametrization task returned with status code: " + String.valueOf(responseEntity.getStatusCode()) + " - " + responseEntity.getStatusCode().name());
                    if (responceBody != null) {
                        Log.i(Constants.LOG, "TID:" + transaction + "| Async parametrization task content: " + responseEntity.getBody());
                        return responceBody;
                    }
                    else {
                        Log.e(Constants.LOG, "TID:" + transaction + "| Async parametrization task content is null");
                        return null;
                    }
                }
                else {
                    Log.e(Constants.LOG, "TID:" + transaction + "| Async parametrization task returned null");
                    return null;
                }

        } catch (RestClientException e) {
            //HandleNetworkError(e);
            if (BuildConfig.DEBUG) {
                Log.d(Constants.LOG, e.toString());
            }
            //e.printStackTrace();
            return null;
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.e(Constants.LOG, e.toString());
            }
            return null;
        }
    }

    // ToDo
    private void HandleNetworkError(RestClientException e) {

    }

    protected void onProgressUpdate(Integer... progress) {
        //setProgressPercent(progress[0]);
        if (BuildConfig.DEBUG) {
            Log.d(Constants.LOG, "Progress: " + progress[0] + "%");
        }
    }
}