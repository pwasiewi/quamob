package com.kp.quasarspectrum.Model.Property;

public class Element {
	
	private String name;
	public String getName() { return name; }
	
	private double[] range;
	public double[] getRange() { return range; }
	
	private double[] fitGuess;
	public double[] getFitGuess() { return fitGuess; }
		
	public Element () {}
	
	public Element (String name, double[] range, double[] fitGuess)
	{
		this.name = name;
		this.range = range;
		this.fitGuess = fitGuess;
	}
}
