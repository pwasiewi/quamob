package com.kp.quasarspectrum.Model.Property;


public class ResultOptions {

	private Double ampWavelength;
	private String continuumWindowsFile;
    private ArrayValuePair<Float>[] continuumWindows;
    private String elementsFile;
    private Element[] elements;
    private String feTemplateFile;
    private String feWindowsFile;
   	private ArrayValuePair<Float>[] feWindows;
    private FeFitParameters feFitParameters;

        
    
	public ResultOptions() {}



	public ResultOptions(Double ampWavelength, String continuumWindowsFile, ArrayValuePair<Float>[] continuumWindows,
			String elementsFile, Element[] elements, String feTemplateFile, String feWindowsFile,
			ArrayValuePair<Float>[] feWindows, FeFitParameters feFitParameters) {
		this.ampWavelength = ampWavelength;
		this.continuumWindowsFile = continuumWindowsFile;
		this.continuumWindows = continuumWindows;
		this.elementsFile = elementsFile;
		this.elements = elements;
		this.feTemplateFile = feTemplateFile;
		this.feWindowsFile = feWindowsFile;
		this.feWindows = feWindows;
		this.feFitParameters = feFitParameters;
	}



	public Double getAmpWavelength() {
		return ampWavelength;
	}



	public void setAmpWavelength(Double ampWavelength) {
		this.ampWavelength = ampWavelength;
	}



	public String getContinuumWindowsFile() {
		return continuumWindowsFile;
	}



	public void setContinuumWindowsFile(String continuumWindowsFile) {
		this.continuumWindowsFile = continuumWindowsFile;
	}



	public ArrayValuePair<Float>[] getContinuumWindows() {
		return continuumWindows;
	}



	public void setContinuumWindows(ArrayValuePair<Float>[] continuumWindows) {
		this.continuumWindows = continuumWindows;
	}



	public String getElementsFile() {
		return elementsFile;
	}



	public void setElementsFile(String elementsFile) {
		this.elementsFile = elementsFile;
	}



	public Element[] getElements() {
		return elements;
	}



	public void setElements(Element[] elements) {
		this.elements = elements;
	}



	public String getFeTemplateFile() {
		return feTemplateFile;
	}



	public void setFeTemplateFile(String feTemplateFile) {
		this.feTemplateFile = feTemplateFile;
	}



	public String getFeWindowsFile() {
		return feWindowsFile;
	}



	public void setFeWindowsFile(String feWindowsFile) {
		this.feWindowsFile = feWindowsFile;
	}



	public ArrayValuePair<Float>[] getFeWindows() {
		return feWindows;
	}



	public void setFeWindows(ArrayValuePair<Float>[] feWindows) {
		this.feWindows = feWindows;
	}



	public FeFitParameters getFeFitParameters() {
		return feFitParameters;
	}



	public void setFeFitParameters(FeFitParameters feFitParameters) {
		this.feFitParameters = feFitParameters;
	}


	
}
