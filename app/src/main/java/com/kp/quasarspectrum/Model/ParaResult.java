package com.kp.quasarspectrum.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kp.quasarspectrum.Interface.AstroObject;
import com.kp.quasarspectrum.Model.Property.ElementFits;

public class ParaResult implements AstroObject{

    private String id;
    public String getId() { return id; }

    @JsonProperty("paraResultSetOid")
    private String para_result_set_oid;
    public String getParaResultSetOid() { return para_result_set_oid; }

    private int mjd;
    public int getMjd() { return mjd; }

    private int plate;
    public int getPlate() { return plate; }

    private int fiber;
    public int getFiber() { return fiber; }

    @JsonProperty("contChisq")
    private double cont_chisq;
    public double getContChisq() { return cont_chisq; }

    @JsonProperty("contReglinResult")
    private double[] cont_reglin_result;
    public double[] getContReglinResult() { return cont_reglin_result; }
    public String getContReglinResultString() {
        String str = "[";

        for(int i = 0; i < cont_reglin_result.length; i++) {
            //str += String.format("%f", cont_reglin_result[i]);
            str += String.format("%s", cont_reglin_result[i]);
            if (i + 1 != cont_reglin_result.length) str += ", ";
        }
        str += "]";

        return str;
    }


    @JsonProperty("reglinResult")
    private double[] reglin_result;
    public double[] getReglinResult() { return reglin_result; }
    public String getReglinResultString() {
        String str = "[";

        for(int i = 0; i < reglin_result.length; i++) {
            //str += String.format("%f", reglin_result[i]);
            str += String.format("%s", reglin_result[i]);
            if (i + 1 != reglin_result.length) str += ", ";
        }
        str += "]";

        return str;
    }

    @JsonProperty("feScaleRate")
    private double fe_scaleRate;
    public double getFeScaleRate() { return fe_scaleRate; }

    @JsonProperty("feWinsSize")
    private int fe_wins_size;
    public int getFeWinsSize() { return fe_wins_size; }

    @JsonProperty("feWinsReducedChisq")
    private double fe_wins_reduced_chisq;
    public double getFeWinsReducedChisq() { return fe_wins_reduced_chisq; }

    @JsonProperty("feFullReducedChisq")
    private double fe_full_reduced_chisq;
    public double getFeFullReducedChisq() { return fe_full_reduced_chisq; }

    @JsonProperty("feFullEw")
    private double fe_full_ew;
    public double getFeFullEw() { return fe_full_ew; }

    @JsonProperty("feRangeReducedChisq")
    private double fe_range_reduced_chisq;
    public double getFeRangeReducedChisq() { return fe_range_reduced_chisq; }

    @JsonProperty("feRangeEw")
    private double fe_range_ew;
    public double getFeRangeEw() { return fe_range_ew; }

    @JsonProperty("elementsFits")
    private ElementFits[] elements_fits;
    public ElementFits[] getElementsFits() { return elements_fits; }


    public ParaResult() {}

    public ParaResult(String id, String para_result_set_oid, int mjd, int plate, int fiber, double cont_chisq,
                      double[] cont_reglin_result, double[] reglin_result, double fe_scaleRate, int fe_wins_size,
                      double fe_wins_reduced_chisq, double fe_full_reduced_chisq, double fe_full_ew,
                      double fe_range_reduced_chisq, double fe_range_ew, ElementFits[] elements_fits) {
        this.id = id;
        this.para_result_set_oid = para_result_set_oid;
        this.mjd = mjd;
        this.plate = plate;
        this.fiber = fiber;
        this.cont_chisq = cont_chisq;
        this.cont_reglin_result = cont_reglin_result;
        this.reglin_result = reglin_result;
        this.fe_scaleRate = fe_scaleRate;
        this.fe_wins_size = fe_wins_size;
        this.fe_wins_reduced_chisq = fe_wins_reduced_chisq;
        this.fe_full_reduced_chisq = fe_full_reduced_chisq;
        this.fe_full_ew = fe_full_ew;
        this.fe_range_reduced_chisq = fe_range_reduced_chisq;
        this.fe_range_ew = fe_range_ew;
        this.elements_fits = elements_fits;
    }
	
    @Override
    public String toString()
    { 
    	//return String.format("ParaResult: id = %s, set_id = %s, mjd = %d, plate = %d, fiber = %d, elements = %d", id, para_result_set_oid, mjd, plate, fiber, elements_fits.length);
        //return String.format("MJD = %d, Plate = %d, Fiber = %d, elements = %d", mjd, plate, fiber, elements_fits.length);

        String elements = "";
        for (ElementFits ef : elements_fits) {
            elements += ef.getName() + " ";
        }

        return String.format("PR: MJD = %d, Plate = %d, Fiber = %d, Elements count = %d ( %s)", mjd, plate, fiber, elements_fits.length, elements);
    }
}