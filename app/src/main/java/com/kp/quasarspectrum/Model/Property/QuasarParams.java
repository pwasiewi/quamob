package com.kp.quasarspectrum.Model.Property;

import java.io.Serializable;

public class QuasarParams implements Serializable {
	private String name;
	public String getName() { return name; }
	
	private String type;
	public String getType() { return type; }
	
	private double z;
	public double getZ() { return z; }
	
	private double ra;
	public double getRa() { return ra; }
	
	private double dec;
	public double getDec() { return dec; }
	
	private int mjd;
	public int getMjd() { return mjd; }
	
	private int plate;
	public int getPlate() { return plate; }
	
	private int fiber;
	public int getFiber() { return fiber; }
	
	private double a;
	public double getA() { return a; }
	
	private double b;
	public double getB() { return b; }
	
	public QuasarParams() {}
	
	public QuasarParams(String name, String type, double z, double ra, double dec, int mjd, int plate, int fiber, double a, double b)
	{
		this.name = name;
		this.type = name;
		this.z = z;
		this.ra = ra;
		this.dec = dec;
		this.mjd = mjd;
		this.plate = plate;
		this.fiber = fiber;
		this.a = a;
		this.b = b;
	}
	
}
