package com.kp.quasarspectrum.Model;

import java.io.Serializable;

public class QuasarSet implements Serializable {

    private String id;
    public String getId() { return id; }

    private String name;
    public String getName() { return name; }

    private String date;
    public String getDate() { return date; }

    private String insertDate;
    public String getInsertDate() { return insertDate; }

    private int size;
    public int getSize() { return size; }

    public QuasarSet() {}

    public QuasarSet(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public QuasarSet(String id, String name, String date, String insertDate, int size) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.insertDate = insertDate;
        this.size = size;
    }

    @Override
    public String toString()
    {
        //return String.format("QuasarSet: name = %s, date = %s, insertDate = %s, size = %d, id = %s", name, date, insertDate, size, id);
    	return String.format("AOS: Name = %s, Date = %s, Size = %d", name, date, size);
    }

    
}
