package com.kp.quasarspectrum.Model.Property;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ElementFits {
	
	private String name;
	public String getName() { return name; }

	@JsonProperty("gaussianParams")
	private double[] gaussian_params;
	public double[] getGaussianParams() { return gaussian_params; }

	@JsonProperty("gaussianFwhm")
	private double gaussian_fwhm;
	public double getGaussianFwhm() { return gaussian_fwhm; }

	private double chisq;
	public double getChisq() { return chisq; }

	private double ew;
	public double getEw() { return ew; }
		
	public ElementFits () {}
	
	public ElementFits(String name, double[] gaussian_params, double gaussian_fwhm, double chisq, double ew) {
		this.name = name;
		this.gaussian_params = gaussian_params;
		this.gaussian_fwhm = gaussian_fwhm;
		this.chisq = chisq;
		this.ew = ew;
	}

	@Override
	public String toString()
	{
		String str = "[";

		for(int i = 0; i < gaussian_params.length; i++) {
			str += String.format("%f", gaussian_params[i]);
			if (i + 1 != gaussian_params.length) str += ",";
		}
		str += "]";

		return String.format("Element: name = %s, gaussian params = %s, gaussian fwhm = %f, chisq = %f, ew = %f", name, str, gaussian_fwhm, chisq, ew);
	}
}
