package com.kp.quasarspectrum.Model.Command;

public class SpectralLine {
	// Ex: { std::string("Ha"), { 6480.0f, 6660.0f }, { 5.0e-17, 6562.8f, 20.0f, 0.0f} },

	private String name;
	private float[] range;
	private float[] fitGuess;
	
	public SpectralLine() {}

	public SpectralLine(String name, float[] range, float[] fitGuess) {
		this.name = name;
		this.range = range;
		this.fitGuess = fitGuess;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float[] getRange() {
		return range;
	}

	public void setRange(float[] range) {
		this.range = range;
	}

	public float[] getFitGuess() {
		return fitGuess;
	}

	public void setFitGuess(float[] fitGuess) {
		this.fitGuess = fitGuess;
	}
	
	public String toFileString() {
		return this.toFileString("\t");
	}
	
	public String toFileString(String separator) {
		String ret = "";
		ret += name; 
		ret += separator;
		ret += this.range[0];
		ret += separator;
		ret += this.range[1];
		ret += separator;
		ret += this.fitGuess[0];
		ret += separator;
		ret += this.fitGuess[1];
		ret += separator;
		ret += this.fitGuess[2];
		ret += separator;
		ret += this.fitGuess[3];			
	
		return ret;
	}
	
	@Override
	public String toString() {
		String ret = "SpectralLine: name = " + this.getName() + ", ";
		ret += "range = ["; 
		ret += this.range[0];
		ret += ",";
		ret += this.range[1];
		ret += "], fitGuess = [";
		ret += this.fitGuess[0];
		ret += ",";
		ret += this.fitGuess[1];
		ret += ",";
		ret += this.fitGuess[2];
		ret += ",";
		ret += this.fitGuess[3];	
		ret += "]";
		
		return ret;
	}
}