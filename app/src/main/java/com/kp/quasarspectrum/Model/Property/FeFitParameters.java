package com.kp.quasarspectrum.Model.Property;

public class FeFitParameters {
	
	private Double feScaleRate;
	private Double fwhmn;
	private Double fwhmt;
	private Integer fitType;
	private Double[] feFitRange;
	
	public FeFitParameters () {}

	public FeFitParameters(Double feScaleRate, Double fwhmn, Double fwhmt, Integer fitType, Double[] feFitRange) {
		this.feScaleRate = feScaleRate;
		this.fwhmn = fwhmn;
		this.fwhmt = fwhmt;
		this.fitType = fitType;
		this.feFitRange = feFitRange;
	}

	public Double getFeScaleRate() {
		return feScaleRate;
	}

	public void setFeScaleRate(Double feScaleRate) {
		this.feScaleRate = feScaleRate;
	}

	public Double getFwhmn() {
		return fwhmn;
	}

	public void setFwhmn(Double fwhmn) {
		this.fwhmn = fwhmn;
	}

	public Double getFwhmt() {
		return fwhmt;
	}

	public void setFwhmt(Double fwhmt) {
		this.fwhmt = fwhmt;
	}

	public Integer getFitType() {
		return fitType;
	}

	public void setFitType(Integer fitType) {
		this.fitType = fitType;
	}

	public Double[] getFeFitRange() {
		return feFitRange;
	}

	public void setFeFitRange(Double[] feFitRange) {
		this.feFitRange = feFitRange;
	}

}
