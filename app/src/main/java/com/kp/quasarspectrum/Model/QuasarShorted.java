package com.kp.quasarspectrum.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kp.quasarspectrum.Interface.AstroObject;
import com.kp.quasarspectrum.Model.Property.QuasarParams;

public class QuasarShorted implements AstroObject {

    private String id;
    public String getId() { return id; }

    @JsonProperty("quasarSetOid")
    private String quasar_set_oid;
    public String getQuasarSetOid() { return quasar_set_oid; }

    private QuasarParams params;
    public QuasarParams getParams() { return params; }

    private int size;
    public long getSize() { return size; }

    public QuasarShorted() {}

    public QuasarShorted(String id, String quasar_set_oid, QuasarParams params, int size) {
        this.id = id;
        this.quasar_set_oid = quasar_set_oid;
        this.params = params;
        this.size = size;
    }

    @Override
    public String toString()
    {
        //return String.format("Quasar: name = %s, id = %s, size = %d", params.getName(), id, size);
        return String.format("AO: MJD = %d, Plate = %d, Fiber = %d, Name = %s, Type = %s", params.getMjd(), params.getPlate(), params.getFiber(), params.getName(), params.getType());
    }

    
}
