package com.kp.quasarspectrum.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kp.quasarspectrum.Interface.AstroObject;
import com.kp.quasarspectrum.Model.Property.ResultOptions;

public class ParaResultSet implements AstroObject {

	private String id;
	public String getId() { return id; }

	private String name;
	public String getName() { return name; }

	@JsonProperty("set_name")
	private String set_name;
	public String GetSetName() { return set_name; }

	private String date;
	public String getDate() { return date; }

	private int size;
	public int getSize() { return size; }

	private ResultOptions resultOptions;
	public ResultOptions getResultOptions() { return resultOptions; }

	private String description;
	public String getDescription() { return description; }


	public ParaResultSet() {}

	public ParaResultSet(String id, String name, String set_name, String date, int size, ResultOptions resultOptions,
						 String description) {
		this.id = id;
		this.name = name;
		this.set_name = set_name;
		this.date = date;
		this.size = size;
		this.resultOptions = resultOptions;
		this.description = description;
	}

    @Override
    public String toString()
    {    	
    	//return String.format("ParaResultSet: set_name = %s, name = %s, date = %s, size = %d, id = %s", set_name, name, date, size, id);

		// ToDo include parametrization option files' names?
		return String.format("PRS: Set name = %s, date = %s", set_name, date);
    }

    
}
