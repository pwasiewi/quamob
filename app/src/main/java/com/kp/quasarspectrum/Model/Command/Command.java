package com.kp.quasarspectrum.Model.Command;

import java.io.Serializable;

//import java.time.Instant;

public class Command implements Serializable{

	private Boolean useDefault = true;
	private String setName;
	private SpectralLine[] spectralLines;
	private Float[][] continuumWindows;
	private Float ampWl;
	private Float[][] feWindows;
	private Float[][] feTemp;
	private Float feFwhmn;
	private Float feFwhmt;
	private Float feScaleRate;
	private Float[] feFitRanges;
	private Integer feFitType;
	//private int n_objs; - not accepted by quasar application due to input parameter error. Not required.

	public Boolean getUseDefault() {
		return useDefault;
	}
	public void setUseDefault(Boolean useDefault) {
		this.useDefault = useDefault;
	}
	public String getSetName() {
		return setName;
	}
	public void setSetName(String setName) {
		this.setName = setName;
	}
	public SpectralLine[] getSpectralLines() {
		return spectralLines;
	}
	public void setSpectralLines(SpectralLine[] spectralLines) {
		this.spectralLines = spectralLines;
	}
	public Float[][] getContinuumWindows() {
		return continuumWindows;
	}
	public void setContinuumWindows(Float[][] continuumWindows) {
		this.continuumWindows = continuumWindows;
	}
	public Float getAmpWl() {
		return ampWl;
	}
	public void setAmpWl(Float ampWl) {
		this.ampWl = ampWl;
	}
	public Float[][] getFeWindows() {
		return feWindows;
	}
	public void setFeWindows(Float[][] feWindows) {
		this.feWindows = feWindows;
	}
	public Float[][] getFeTemp() {
		return feTemp;
	}
	public void setFeTemp(Float[][] feTemp) {
		this.feTemp = feTemp;
	}
	public Float getFeFwhmn() {
		return feFwhmn;
	}
	public void setFeFwhmn(Float feFwhmn) {
		this.feFwhmn = feFwhmn;
	}
	public Float getFeFwhmt() {
		return feFwhmt;
	}
	public void setFeFwhmt(Float feFwhmt) {
		this.feFwhmt = feFwhmt;
	}
	public Float getFeScaleRate() {
		return feScaleRate;
	}
	public void setFeScaleRate(Float feScaleRate) {
		this.feScaleRate = feScaleRate;
	}
	public Float[] getFeFitRanges() {
		return feFitRanges;
	}
	public void setFeFitRanges(Float[] feFitRanges) {
		this.feFitRanges = feFitRanges;
	}

	public Integer getFeFitType() {
		return feFitType;
	}

	public void setFeFitType(Integer feFitType) {
		this.feFitType = feFitType;
	}

	public Command() {
	}

	public Command(Boolean useDefault) {
		this.useDefault = useDefault;
	}

	public Command(Boolean useDefault, String setName, SpectralLine[] spectralLines,
				   Float[][] continuumWindows, Float ampWl, Float[][] feWindows, Float[][] feTemp,
				   Float feFwhmn, Float feFwhmt, Float feScaleRate, Float[] feFitRanges, Integer feFitType) {

		this.useDefault = useDefault;
		this.setName = setName;
		this.spectralLines = spectralLines;
		this.continuumWindows = continuumWindows;
		this.ampWl = ampWl;
		this.feWindows = feWindows;
		this.feTemp = feTemp;
		this.feFwhmn = feFwhmn;
		this.feFwhmt = feFwhmt;
		this.feScaleRate = feScaleRate;
		this.feFitRanges = feFitRanges;
		this.feFitType = feFitType;
	}

	@Override
	public String toString() {
		String str = "Command: (";
		str += "useDefault = " + useDefault.toString() + ", ";
		if (setName != null) str += "setName = " + setName + ", ";
		if (ampWl != null) str += "ampWl = " + ampWl + ", ";
		if (feFwhmn != null) str += "feFwhmn = " + feFwhmn + ", ";
		if (feFwhmt != null) str += "feFwhmt = " + feFwhmt + ", ";
		if (feScaleRate != null) str += "feScaleRate = " + feScaleRate + ", ";

		if (spectralLines != null)
		{
			int i = 0;
			for(SpectralLine sl : spectralLines)
			{
				str += String.format(" spectralLines[%d] = ", i);
				str += sl.toString();
				i ++;
			}
		}

		if (continuumWindows != null)
		{
			str += " continuumWindows = ";
			for (int j = 0; j < continuumWindows.length; j++)
			{
				str += "[";
				for (int k = 0; k < continuumWindows[j].length; k++)
				{
					str += String.format("%f", continuumWindows[j][k]);
					if (!(k + 1 == continuumWindows[j].length)) str += ",";
				}
				str += "]";
			}
		}

		if (feWindows != null)
		{
			str += " feWindows = ";
			for (int j = 0; j < feWindows.length; j++)
			{
				str += "[";
				for (int k = 0; k < feWindows[j].length; k++)
				{
					str += String.format("%f", feWindows[j][k]);
					if (!(k + 1 == feWindows[j].length)) str += ",";
				}
				str += "]";
			}
		}

		if (feTemp != null)
		{
			str += " feTemp = ";
			for (int j = 0; j < feTemp.length; j++)
			{
				str += "[";
				for (int k = 0; k < feTemp[j].length; k++)
				{
					str += String.format("%f", feTemp[j][k]);
					if (!(k + 1 == feTemp[j].length)) str += ",";
				}
				str += "]";
			}
		}

		if (feFitRanges != null)
		{
			str += " feFitRanges = ";
			str += "[";
			for (int j = 0; j < feFitRanges.length; j++)
			{
				str += String.format("%f", feFitRanges[j]);
				if (!(j+1 == feFitRanges.length))
					str += ",";
			}
			str += "]";
		}

		if (feFitType != null)
		{
			str += " feFitType = ";
			switch(feFitType){
				case 1: { str += "FULL"; break; }
				case 2: { str += "WIN"; break; }
				case 3: { str += "FWIN"; break; }
			}
		}

		str += ")";
		return str;
	}

	// ToDo
	public Boolean ValidateData() {
		return true;
	}

/*	public CommandParameters PrepareParaCommand() {
		CommandParameters commandParameters = new CommandParameters();
		Map<String, Object> parameters = new HashMap<String, Object>();

		String command = config.getQuasarExecFile();
		command += " -v";
		command += " " + config.getMongoDB();
		command += " -t para";
		log.info(String.format("Using database at: %s", config.getMongoDB()));

		if (setName != null)
		{
			command += " --set_name " + setName.toString();
			parameters.put("set_name", setName.toString());
		}
		else return new CommandParameters(true);

		Instant instant = Instant.now(); // Current date-time in UTC.

		commandParameters.setDate(instant.toString());

		// Custom parametrization
		if (!useDefault)
		{
			log.info("Using custom parametrization values");
			if (this.ValidateData())	// ToDo validate data
			{
				if (ampWl != null)
				{
					command += " --amp_wl " + ampWl.toString();
					parameters.put("amp_wl", ampWl);
				}
				else parameters.put("amp_wl", new Float(3000.0));			// Setting default value as withing quasar application

				if (feFwhmn != null)
				{
					command += " --fe_fwhmn " + feFwhmn.toString();
					parameters.put("fe_fwhmn", feFwhmn);
				}
				else parameters.put("fe_fwhmn", new Float(1600.0));			// Setting default value as withing quasar application

				if (feFwhmt != null)
				{
					command += " --fe_fwhmt " + feFwhmt.toString();
					parameters.put("fe_fwhmt", feFwhmt);
				}
				else parameters.put("fe_fwhmt", new Float(900.0));			// Setting default value as withing quasar application

				if (feScaleRate != null)
				{
					command += " --fe_scale_rate " + feScaleRate.toString();
					parameters.put("fe_scale_rate", feScaleRate);
				}
				else parameters.put("fe_scale_rate", new Float(1.0));		// Setting default value as withing quasar application

				if (feFitType != null)
				{
					// 3 options: FULL = 1, WIN = 2, FWIN = 3
					switch(feFitType){
						case 1: {command += " --fe_fit_type FULL"; parameters.put("fe_fit_type", new Integer(1)); break;}
						case 2: {command += " --fe_fit_type WIN"; parameters.put("fe_fit_type", new Integer(2)); break;}
						case 3: {command += " --fe_fit_type FWIN"; parameters.put("fe_fit_type", new Integer(3)); break;}
					}
				}
				else parameters.put("fe_fit_type", new Integer(2));				// Setting default value as withing quasar application

				// Files
				SaveResult saveResult;

				if (spectralLines != null && spectralLines.length != 0)
				{
					saveResult = SaveSpectralLines(); // SL
					if (saveResult.getResult() == true)
					{
						command += " --spectral_lines " + saveResult.getFileName();
						parameters.put("spectral_lines", saveResult.getFileName());
					}
					else
					{
						// return "failed"; // ?
						log.warn("Saving spectralLines failed. Using default.");
						parameters.put("spectral_lines", "built-in");
					}
				}
				else parameters.put("spectral_lines", "built-in");

				if (continuumWindows != null && continuumWindows.length != 0)
				{
					saveResult = SaveArray(continuumWindows, "CW");
					if (saveResult.getResult() == true)
					{
						command += " --cont_wins " + saveResult.getFileName();
						parameters.put("cont_wins", saveResult.getFileName());
					}
					else
					{
						log.warn("Saving continuumWindows failed. Using default.");
						parameters.put("cont_wins", "built-in");
					}
				}
				else parameters.put("cont_wins", "built-in");

				if (feWindows != null && feWindows.length != 0)
				{
					saveResult = SaveArray(feWindows, "FW");
					if (saveResult.getResult() == true)
					{
						command += " --fe_wins " + saveResult.getFileName();
						parameters.put("fe_wins", saveResult.getFileName());
					}
					else
					{
						log.warn("Saving feWindows failed. Using default.");
						parameters.put("fe_wins", "built-in");
					}
				}
				else parameters.put("fe_wins", "built-in");

				if (feFitRanges != null && feFitRanges.length != 0)
				{
					log.warn("Argument not supported. Skipping.");
					// ToDo: how to pass cl_float2 in quasar command? Nothing works...
					*//*
					command += " --fe_fit_range [";
					for (int j = 0; j < feFitRanges.length; j++)
					{
						command += String.format("%f", feFitRanges[j]);
						if (!(j+1 == feFitRanges.length))
							command += ",";
					}
					command += "]";
					// parameters.put("fe_fit_range", );
					*//*
					parameters.put("fe_fit_range", "[2200.0, 2650.0]");
				}
				else parameters.put("fe_fit_range", "[2200.0, 2650.0]");

				// Required by quasar application. Default (from quasar test) values will be passed if feTemp is null.
				if (feTemp != null && feTemp.length != 0)
				{
					saveResult = SaveArray(feTemp, "FT");
					if (saveResult.getResult() == true)
					{
						command += " --fe_temp " + saveResult.getFileName();
						parameters.put("fe_temp", saveResult.getFileName());
					}
					else
					{
						log.warn("Saving feTemp failed. Using default.");
						command += " --fe_temp " + config.getDefaultFeTemplateFile();
						parameters.put("fe_temp", config.getDefaultFeTemplateFile());
					}
				}
				else
				{
					command += " --fe_temp " + config.getDefaultFeTemplateFile();
					parameters.put("fe_temp", config.getDefaultFeTemplateFile());
				}
			}
			else
			{
				log.error("Validating data failed");
				return new CommandParameters(true);
			}
		}
		else
		{
			log.info("Using default parametrization values");
			commandParameters.setUseDefault(true);
			command += " --fe_temp " + config.getDefaultFeTemplateFile();
			parameters.put("fe_temp", config.getDefaultFeTemplateFile());
		}

		commandParameters.setCommand(command);
		commandParameters.setParameters(parameters);

		// Saving parametrization info (for debugging)
		SaveCommandParameters(commandParameters);

		return commandParameters;
	}

	public SaveResult SaveArray(Float[][] array, String dataType)
	{
		// Get time
		Instant instant = Instant.now(); // Current date-time in UTC.
		Long mili = instant.toEpochMilli();
		String fileName = dataType + "_" + mili.toString();
		fileName += ".txt";

		// Get path
		String filePath = config.getCommandTempPath();
		filePath += fileName;

		String line;
		try(  PrintWriter out = new PrintWriter( filePath )  ){
			for (int i = 0; i<array.length; i++)
			{
				line = "";
				for (int j = 0; j<array[i].length; j++)
				{
					line += array[i][j].toString();
					if (!(j + 1 == array[i].length))
						line += separator;
				}
				out.println(line);
			}
		}
		catch (java.io.FileNotFoundException e)
		{
			log.error(e.toString());
			return new SaveResult(false, "");
		}
		catch (Exception e)
		{
			log.error(e.toString());
			return new SaveResult(false, "");
		}

		log.debug(String.format("Saved %s array file: %s", dataType, filePath));

		return new SaveResult(true, filePath);
	}

	public SaveResult SaveSpectralLines()
	{
		// Get time
		Instant instant = Instant.now(); // Current date-time in UTC.
		Long mili = instant.toEpochMilli();
		String fileName = "SL_" + mili.toString();
		fileName += ".txt";

		// Get path
		String filePath = config.getCommandTempPath();
		filePath += fileName;

		try(  PrintWriter out = new PrintWriter( filePath )  ){
			for (int i = 0; i<this.getSpectralLines().length; i++)
			{
				out.println(this.getSpectralLines()[i].toFileString(separator));
			}
		}
		catch (java.io.FileNotFoundException e)
		{
			log.error(e.toString());
			return new SaveResult(false, "");
		}
		catch (Exception e)
		{
			log.error(e.toString());
			return new SaveResult(false, "");
		}

		log.debug(String.format("Saved SL file: %s", filePath));

		return new SaveResult(true, filePath);
	}

	public Boolean SaveCommandParameters(CommandParameters commandParameters)
	{
		// Get time
		Instant instant = Instant.now(); // Current date-time in UTC.
		Long mili = instant.toEpochMilli();
		String fileName = "CP_" + mili.toString();
		fileName += ".txt";

		// Get path
		String filePath = config.getCommandTempPath();
		filePath += fileName;

		Map<String, Object> parameters = commandParameters.getParameters();


		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = "";
		try {
			json = ow.writeValueAsString(commandParameters);
		}
    	catch (JsonGenerationException e)
    	{
			e.printStackTrace();
		}
    	catch (JsonMappingException e)
    	{
			e.printStackTrace();
		}
		catch (JsonProcessingException e) {

			e.printStackTrace();
		}

		try(  PrintWriter out = new PrintWriter( filePath )  ){
			if (json != null )
				out.print(json);
		}
		catch (java.io.FileNotFoundException e)
		{
			log.error(e.toString());
			return false;
		}
		catch (Exception e)
		{
			log.error(e.toString());
			return false;
		}

		log.debug(String.format("Saved CP file: %s", filePath));
		//log.debug(String.format("Saved CP file: %s\n%s", filePath, json));
		//log.debug(String.format("CP file: \n%s",json));

		return true;
	}*/

	/*
	// Examples from quasar application
	("set_name,N",
			po::value<std::string>()->required(),
			"Astronomical objects set name (in database).")
				fileList_0354_51792.par

	("spectral_lines",
			po::value<std::string>(),
			"Path to a file containing spectral lines.")
				{ std::string("Ha"), { 6480.0f, 6660.0f }, { 5.0e-17, 6562.8f, 20.0f, 0.0f} },
				{ std::string("Hb"), { 4800.0f, 4900.0f }, { 1.0e-17, 4861.0f, 20.0f, 0.0f} },
				{ std::string("MgII"), { 2750.0f, 2850.0f }, { 1.0e-17, 2800.0f, 20.0f, 0.0f} },
				{ std::string("CIII"), { 1880.0f, 1930.0f }, { 5.0e-17, 1909.0f, 10.0f, 0.0f} },
				{ std::string("CIV"), { 1520.0f, 1570.0f }, { 5.0e-17, 1549.0f, 10.0f, 0.0f} },
				{ std::string("SiIV"), { 1370.0f, 1430.0f }, { 1.0e-17, 1400.0f, 10.0f, 0.0f} },
				{ std::string("Lya"), { 1190.0f, 1250.0f }, { 5.0e-17, 1216.0f, 20.0f, 0.0f} }

	("cont_wins",
			po::value<std::string>(),
			"Path to a file containing continuum spectral windows (wavelength intervals [A]).")
				{ 1140., 1150. },
				{ 1275., 1280. },
				{ 1320., 1330. },
				{ 1455., 1470. },
				{ 1690., 1700. },
				{ 2160., 2180. },
				{ 2225., 2250. },
				{ 3010., 3040. },
				{ 3240., 3270. },
				{ 3790., 3810. },
				{ 4210., 4230. },
				{ 5080., 5100. },
				{ 5600., 5630. },
				{ 5970., 6000. }

	("amp_wl",
			po::value<float>()->default_value(amp_wl_default),
			"Continuum power-law function amplitude wavelength [A].")
				// Default 3000.0f
	("fe_wins",
			po::value<std::string>(),
			"Path to a file containing iron emission template spectral windows (wavelength intervals [A]).")
				{ 2020., 2120. },
				{ 2250., 2650. },
				{ 2900., 3000. },
				{ 4400., 4750. },
				{ 5150., 5500. }

	("fe_temp",
			po::value<std::string>()->required(),
			"Path to a file containing iron emission template.")
				// REQUIRED default is from quasar test "iron_emmision_temp.txt"

	("fe_fwhmn",
			po::value<float>()->default_value(fe_fwhmn_default),
			"Full-Width at Half-Maximum of objects' spectral lines.")
				// Default 1600.0f

	("fe_fwhmt",
			po::value<float>()->default_value(fe_fwhmt_default),
			"Full-Width at Half-Maximum of iron emission template spectral.")
				// Default 900.0f

	("fe_scale_rate",
			po::value<float>()->default_value(fe_scale_rate_default),
			"Additional scale rate of iron emission.")
				// Default 1.0f

	("fe_fit_range",
			po::value<cl_float2>()->default_value(fe_fit_range_default, fe_fit_range_default_text),
			"Main iron emission wavelength interval [A].")
				// Default 2200.0f, 2650.0f and string

	("fe_fit_type",
			po::value<algorithm::ocl::FeFitWin::Type>()->default_value(fe_fit_type_default, fe_fit_type_default_text),
			"Iron template fitting type: FULL, FWIN, WIN.")
				// Default WIN (FULL, FWIN, WIN
				// FULL = 1, WIN = 2, FWIN = 3

	("n_objs" - parameter error (deleted)
	*/
}







