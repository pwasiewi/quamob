package com.kp.quasarspectrum.Model.Command;

/**
 * Created by administrator on 10.06.16.
 */
public class ParametrizationOption {
    private String option;
    private String optionString;
    private String value;

    public Boolean getUsingDefault() {
        return usingDefault;
    }

    public void setUsingDefault(Boolean usingDefault) {
        this.usingDefault = usingDefault;
    }

    private Boolean usingDefault;

    public ParametrizationOption(){}

    public ParametrizationOption(String option, String optionString, String value){
        this(option, optionString, value, true);
    }

    public ParametrizationOption(String option, String optionString, String value, Boolean usingDefault){
        this.option = option;
        this.optionString = optionString;
        this.value = value;
        this.usingDefault = usingDefault;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getOptionString() {
        return optionString;
    }

    public void setOptionString(String optionString) {
        this.optionString = optionString;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }



}
